﻿using CuirassUModelsDBLib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace FreqRangesControl
{
    public partial class UserControlFreqRanges : UserControl
    {
        /// <summary>
        /// Обновить контрол
        /// </summary>
        /// <param name="listFreqRanges"></param>
        public void UpdateFreqRanges(List<TableFreqRanges> listFreqRanges)
        {
            try
            {
                if (listFreqRanges == null)
                    return;

                ((GlobalFreqRanges)DgvFreqRanges.DataContext).CollectionFreqRanges.Clear();

                for (int i = 0; i < listFreqRanges.Count; i++)
                {
                    ((GlobalFreqRanges)DgvFreqRanges.DataContext).CollectionFreqRanges.Add(listFreqRanges[i]);
                }

                AddEmptyRows();

            }
            catch { }
        }

        /// <summary>
        /// Добавить несколько записей в контрол
        /// </summary>
        /// <param name="listFreqRanges"></param>
        public void AddFreqRanges(List<TableFreqRanges> listFreqRanges)
        {
            try
            {
                DeleteEmptyRows();

                for (int i = 0; i < listFreqRanges.Count; i++)
                {
                    int ind = ((GlobalFreqRanges)DgvFreqRanges.DataContext).CollectionFreqRanges.ToList().FindIndex(x => x.Id == listFreqRanges[i].Id);
                    if (ind != -1)
                    {
                        ((GlobalFreqRanges)DgvFreqRanges.DataContext).CollectionFreqRanges[ind] = listFreqRanges[i];
                    }
                    else
                    {
                        ((GlobalFreqRanges)DgvFreqRanges.DataContext).CollectionFreqRanges.Add(listFreqRanges[i]);
                    }
                }

                AddEmptyRows();
            }
            catch { }
        }

        /// <summary>
        /// Удалить одну запись из контрола
        /// </summary>
        public void DeleteFreqRanges(TableFreqRanges tableFreqRanges)
        {
            try
            {
                int index = ((GlobalFreqRanges)DgvFreqRanges.DataContext).CollectionFreqRanges.ToList().FindIndex(x => x.Id == tableFreqRanges.Id);
                ((GlobalFreqRanges)DgvFreqRanges.DataContext).CollectionFreqRanges.RemoveAt(index);

                AddEmptyRows();
            }
            catch { }
        }

        /// <summary>
        /// Удалить все записи из контрола
        /// </summary>
        public void ClearFreqRanges()
        {
            try
            {
                ((GlobalFreqRanges)DgvFreqRanges.DataContext).CollectionFreqRanges.Clear();

                AddEmptyRows();
            }
            catch { }
        }

        /// <summary>
        /// Добавить в таблицу пустые строки
        /// </summary>
        private void AddEmptyRows()
        {
            try
            {
                int сountRowsAll = DgvFreqRanges.Items.Count; // количество всех строк в таблице
                double hs = 23; // высота строки
                double ah = DgvFreqRanges.ActualHeight; // визуализированная высота dataGrid
                double chh = DgvFreqRanges.ColumnHeaderHeight; // высота заголовка

                int countRows = Convert.ToInt32(Math.Floor(Convert.ToDouble(ah) - Convert.ToDouble(chh)) / hs); // количество строк, которое помещается в область видимости = (высота таблицы - заголовок таблицы) / высота строки

                int count = сountRowsAll - countRows; // сколько пустых строк нужно удалить
                int index = -1;
                for (int i = 0; i < count; i++)
                {
                    // Удалить пустые строки в dgv
                    index = ((GlobalFreqRanges)DgvFreqRanges.DataContext).CollectionFreqRanges.ToList().FindIndex(x => x.Id < 0);
                    if (index != -1)
                    {
                        ((GlobalFreqRanges)DgvFreqRanges.DataContext).CollectionFreqRanges.RemoveAt(index);
                    }
                }

                List<TableFreqRanges> list = new List<TableFreqRanges>();
                for (int i = 0; i < countRows - сountRowsAll; i++)
                {
                    TableFreqRanges strFR = new TableFreqRanges
                    {
                        Id = -2,
                        IsActive = false,
                        FreqMinMHz = 0,
                        FreqMaxMHz = 0,
                        Note = string.Empty
                    };
                    list.Add(strFR);
                }

                for (int i = 0; i < list.Count; i++)
                {
                    ((GlobalFreqRanges)DgvFreqRanges.DataContext).CollectionFreqRanges.Add(list[i]);
                }
            }
            catch { }
        }

        /// <summary>
        /// Удалить пустые строки из таблицы
        /// </summary>
        private void DeleteEmptyRows()
        {
            try
            {
                int countEmptyRows = ((GlobalFreqRanges)DgvFreqRanges.DataContext).CollectionFreqRanges.Count(s => s.Id < 0);
                int countAllRows = ((GlobalFreqRanges)DgvFreqRanges.DataContext).CollectionFreqRanges.Count;
                int iCount = countAllRows - countEmptyRows;
                for (int i = iCount; i < countAllRows; i++)
                {
                    ((GlobalFreqRanges)DgvFreqRanges.DataContext).CollectionFreqRanges.RemoveAt(iCount);
                }
            }
            catch { }
        }

        private bool IsSelectedRowEmpty()
        {
            try
            {
                if (((TableFreqRanges)DgvFreqRanges.SelectedItem).Id == -2)
                    return false;
            }
            catch { }

            return true;
        }

        /// <summary>
        /// Определить имя таблицы
        /// </summary>
        /// <returns></returns>
        private NameTable FindNameTableFreqRanges()
        {
            switch (NameTable)
            {
                case FreqRanges.FreqForbidden:
                    return CuirassUModelsDBLib.NameTable.TableFreqForbidden;
                case FreqRanges.FreqRangesRecon:
                    return CuirassUModelsDBLib.NameTable.TableFreqRangesRecon;
                default:
                    break;
            }
            return CuirassUModelsDBLib.NameTable.TableFreqForbidden;
        }

        /// <summary>
        /// Определить тип таблицы
        /// </summary>
        /// <param name="sr"></param>
        /// <returns></returns>
        private AbstractCommonTable FindTypeFreqRanges(TableFreqRanges table)
        {
            switch (NameTable)
            {
                //case FreqRanges.FreqForbidden:
                //    return table.ToFreqForbidden();
                case FreqRanges.FreqRangesRecon:
                    return table.ToRangesRecon();
                default:
                    break;
            }
            return null;
        }
    }
}
