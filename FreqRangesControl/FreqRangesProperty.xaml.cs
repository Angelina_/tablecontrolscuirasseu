﻿using CuirassUModelsDBLib;
using System;
using System.Collections.ObjectModel;
using System.Windows;
using System.Windows.Controls.WpfPropertyGrid;
using System.Windows.Input;
using System.Windows.Media.Imaging;
using ValuesCorrectLib;

namespace FreqRangesControl
{
    using System.IO;
    using System.Linq;

    /// <summary>
    /// Логика взаимодействия для FreqRangesProperty.xaml
    /// </summary>
    public partial class FreqRangesProperty : Window
    {
        private ObservableCollection<TableFreqRanges> collectionTemp;
        public TableFreqRanges FreqRanges { get; private set; }

        public FreqRangesProperty(ObservableCollection<TableFreqRanges> collectionFreqRanges)
        {
            try
            {
                InitializeComponent();

                InitEditors();

                collectionTemp = collectionFreqRanges;
                FreqRanges = new TableFreqRanges();
                propertyGrid.SelectedObject = FreqRanges;

                //Title = "Add record";
                Title = SMeaning.meaningAddRecord;
                Icon = new BitmapImage(new Uri(@"pack://application:,,,/"
                                    + System.Reflection.Assembly.GetExecutingAssembly().GetName().Name
                                    + ";component/"
                                    + "Resources/AddRec.ico", UriKind.Absolute));

                InitProperty();
                //ChangeCategories();
            }
            catch { }
        }

        public FreqRangesProperty(ObservableCollection<TableFreqRanges> collectionFreqRanges, TableFreqRanges tableFreqRanges)
        {
            try
            {
                InitializeComponent();

                InitEditors();

                collectionTemp = collectionFreqRanges;
                FreqRanges = tableFreqRanges;

                FreqRanges.FreqMinMHz = FreqRanges.FreqMinMHz ;
                FreqRanges.FreqMaxMHz = FreqRanges.FreqMaxMHz ;
                propertyGrid.SelectedObject = FreqRanges;

                //Title = "Change record";
                Title = SMeaning.meaningChangeRecord;
                Icon = new BitmapImage(new Uri(@"pack://application:,,,/"
                                    + System.Reflection.Assembly.GetExecutingAssembly().GetName().Name
                                    + ";component/"
                                    + "Resources/ChangeRec.ico", UriKind.Absolute));

                //ChangeCategories();
                InitProperty();
            }
            catch { }
        }

        public FreqRangesProperty()
        {
            InitializeComponent();

            InitEditors();
            //ChangeCategories();
            InitProperty();
        }


        private void ButtonApply_Click(object sender, RoutedEventArgs e)
        {
            if (IsAddClick((TableFreqRanges)propertyGrid.SelectedObject) != null)
            {
                DialogResult = true;
            }
        }

        public TableFreqRanges IsAddClick(TableFreqRanges FreqRangesWindow)
        {
            CorrectParams.IsCorrectMinMax(FreqRangesWindow.FreqMinMHz, FreqRangesWindow.FreqMaxMHz);
            if (CorrectParams.IsCorrectFreqMinMax(FreqRangesWindow.FreqMinMHz, FreqRangesWindow.FreqMaxMHz))
            {
                FreqRangesWindow.IsActive = true;
                return FreqRangesWindow;
            }

            return null;
        }

        private void ButtonNoApply_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = false;
        }

        private void InitEditors()
        {
            propertyGrid.Editors.Add(new FreqRangesFminEditor(nameof(FreqRanges.FreqMinMHz), typeof(TableFreqRanges)));
            propertyGrid.Editors.Add(new FreqRangesFmaxEditor(nameof(FreqRanges.FreqMaxMHz), typeof(TableFreqRanges)));
            propertyGrid.Editors.Add(new FreqRangesNoteEditor(nameof(FreqRanges.Note), typeof(TableFreqRanges)));
        }

        private void InitProperty()
        {
            foreach (var property in propertyGrid.Properties)
            {
                try
                {
                    if (property.IsBrowsable == false) { continue; }

                    if (property.PropertyValue.SubProperties.Count != 0)
                    {
                        foreach (var subProperty in property.PropertyValue.SubProperties)
                            subProperty.PropertyValue.PropertyValueException += PropertyGridSubException;
                        continue;
                    }
                    property.PropertyValue.PropertyValueException += PropertyGridException;
                }
                catch (Exception ex)
                { System.Windows.MessageBox.Show(ex.Message); }
            }
        }

        private void PropertyGridSubException(object sender, ValueExceptionEventArgs e)
        {
            propertyGrid.Properties[(sender as PropertyItemValue).ParentProperty.ParentValue.ParentProperty.Name].SetValue((sender as PropertyItemValue).Value);
        }

        private void PropertyGridException(object sender, ValueExceptionEventArgs e)
        {
            propertyGrid.Properties[(sender as PropertyItemValue).ParentProperty.Name].SetValue((sender as PropertyItemValue).Value);
        }

        private void gridProperty_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == System.Windows.Input.Key.Enter)
            {
                if (IsAddClick((TableFreqRanges)propertyGrid.SelectedObject) != null)
                {
                    DialogResult = true;
                }
            }
        }

        public void SetLanguagePropertyGrid(Languages language)
        {
            LoadTranslatorPropertyGrid(language);
            TranslatorTables.ChangeLanguagePropertyGrid(language, propertyGrid);
        }

        private void LoadTranslatorPropertyGrid(Languages language)
        {
            ResourceDictionary dict = new ResourceDictionary();
            try
            {
                var path = Directory.GetParent(Environment.CurrentDirectory).Parent.Parent.FullName.Split(Path.DirectorySeparatorChar).Last();
                switch (language)
                {
                    case Languages.Eng:
                        dict.Source = new Uri($"/{path};component/Languages/TranslatorTables/TranslatorTablesCuirasse.EN.xaml",
                                      UriKind.Relative);
                        break;
                    case Languages.Rus:
                       
                        dict.Source = new Uri($"/{path};component/Languages/TranslatorTables/TranslatorTablesCuirasse.RU.xaml",
                                            UriKind.Relative);
                        break;
                    default:
                        dict.Source = new Uri($"/{path};component/Languages/TranslatorTables/TranslatorTablesCuirasse.RU.xaml",
                                          UriKind.Relative);
                        break;
                }

                this.Resources.MergedDictionaries.Add(dict);
            }
            catch (Exception ex)
            {
                //MessageBox.Show(ex.Message);
            }
        }
    }
}
