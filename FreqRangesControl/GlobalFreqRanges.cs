﻿using CuirassUModelsDBLib;
using System;
using System.Collections.ObjectModel;
using System.Windows;

namespace FreqRangesControl
{
    public class GlobalFreqRanges
    {
        public ObservableCollection<TableFreqRanges> CollectionFreqRanges { get; set; }

        public GlobalFreqRanges()
        {
            try
            {
                CollectionFreqRanges = new ObservableCollection<TableFreqRanges> { };

                //CollectionFreqRanges = new ObservableCollection<TableFreqRanges>
                //{
                //    new TableFreqRanges
                //    {
                //        Id = 1,
                //        FreqMinMHz = 56999.98,
                //        FreqMaxMHz = 78900.88,
                //        Note = "Jk8 dflk okl"
                //    },
                //    new TableFreqRanges
                //    {
                //        Id = 2,
                //        FreqMinMHz = 455999.58,
                //        FreqMaxMHz = 788900.78,
                //        Note = "jkdsgj 878"
                //    }
                //};
            }
            catch (Exception ex) { MessageBox.Show(ex.Message); }
        }
    }
}
