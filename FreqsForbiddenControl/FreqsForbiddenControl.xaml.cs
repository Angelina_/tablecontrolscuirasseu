﻿using CuirassUModelsDBLib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using TableEvents;

namespace FreqsForbiddenControl
{
    /// <summary>
    /// Логика взаимодействия для UserControl1.xaml
    /// </summary>
    public partial class UserControlFreqsForbidden : UserControl
    {
        public FreqsForbiddenProperty FreqsForbiddenWindow;

        #region Events
        public event EventHandler<TableEvent> OnAddRecord = (object sender, TableEvent data) => { };
        public event EventHandler<TableEvent> OnChangeRecord = (object sender, TableEvent data) => { };
        public event EventHandler<TableEvent> OnDeleteRecord = (object sender, TableEvent data) => { };
        public event EventHandler<NameTable> OnClearRecords = (object sender, NameTable data) => { };

        // Открылось окно с PropertyGrid
        public event EventHandler<FreqsForbiddenProperty> OnIsWindowPropertyOpen = (object sender, FreqsForbiddenProperty data) => { };

        public event EventHandler<TableEventReport> OnAddTableToReport = (object sender, TableEventReport data) => { };
        #endregion
        public UserControlFreqsForbidden()
        {
            InitializeComponent();

            DgvFreqsForbidden.DataContext = new GlobalFreqsForbidden();
        }

        private void ButtonAdd_Click(object sender, RoutedEventArgs e)
        {
            FreqsForbiddenWindow = new FreqsForbiddenProperty(((GlobalFreqsForbidden)DgvFreqsForbidden.DataContext).CollectionFreqsForbidden);

            OnIsWindowPropertyOpen(this, FreqsForbiddenWindow);

            if (FreqsForbiddenWindow.ShowDialog() == true)
            {
                FreqsForbiddenWindow.FreqsForbidden.FreqMinMHz = FreqsForbiddenWindow.FreqsForbidden.FreqMinMHz ;
                FreqsForbiddenWindow.FreqsForbidden.FreqMaxMHz = FreqsForbiddenWindow.FreqsForbidden.FreqMaxMHz ;

                // Событие добавления одной записи
                OnAddRecord(this, new TableEvent(FreqsForbiddenWindow.FreqsForbidden));
            }
        }

        private void ButtonChange_Click(object sender, RoutedEventArgs e)
        {
            if ((TableFreqForbidden)DgvFreqsForbidden.SelectedItem != null)
            {
                if (((TableFreqForbidden)DgvFreqsForbidden.SelectedItem).Id > 0)
                {
                    var selected = (TableFreqForbidden)DgvFreqsForbidden.SelectedItem;

                    FreqsForbiddenWindow = new FreqsForbiddenProperty(((GlobalFreqsForbidden)DgvFreqsForbidden.DataContext).CollectionFreqsForbidden, selected.Clone());

                    OnIsWindowPropertyOpen(this, FreqsForbiddenWindow);

                    if (FreqsForbiddenWindow.ShowDialog() == true)
                    {
                        FreqsForbiddenWindow.FreqsForbidden.FreqMinMHz = FreqsForbiddenWindow.FreqsForbidden.FreqMinMHz ;
                        FreqsForbiddenWindow.FreqsForbidden.FreqMaxMHz = FreqsForbiddenWindow.FreqsForbidden.FreqMaxMHz ;

                        // Событие изменения одной записи
                        OnChangeRecord(this, new TableEvent(FreqsForbiddenWindow.FreqsForbidden));
                    }
                }
            }
        }

        private void ButtonDelete_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if ((TableFreqForbidden)DgvFreqsForbidden.SelectedItem != null)
                {
                    if (!IsSelectedRowEmpty())
                        return;

                    TableFreqForbidden tableFreqsForbidden = new TableFreqForbidden
                    {
                        Id = ((TableFreqForbidden)DgvFreqsForbidden.SelectedItem).Id
                    };

                    // Событие удаления одной записи
                    OnDeleteRecord(this, new TableEvent(tableFreqsForbidden));
                }
            }
            catch { }
        }

        private void ButtonClear_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                // Событие удаления записей
                OnClearRecords(this, NameTable.TableFreqForbidden);
            }
            catch { }
        }

        private void DgvFreqsForbidden_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            AddEmptyRows();
        }

        private void CheckBox_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (((TableFreqForbidden)DgvFreqsForbidden.SelectedItem).Id > 0)
                {
                    if (((TableFreqForbidden)DgvFreqsForbidden.SelectedItem).IsActive)
                    {
                        ((TableFreqForbidden)DgvFreqsForbidden.SelectedItem).IsActive = false;
                    }
                    else
                    {
                        ((TableFreqForbidden)DgvFreqsForbidden.SelectedItem).IsActive = true;
                    }

                    // Событие изменения одной записи
                    OnChangeRecord(this, new TableEvent((TableFreqForbidden)DgvFreqsForbidden.SelectedItem));
                }
                else
                {
                    CheckBox chbIsChecked = sender as CheckBox;
                    chbIsChecked.IsChecked = false;
                }
            }
            catch { }
        }
    }
}
