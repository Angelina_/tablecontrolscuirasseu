﻿using CuirassUModelsDBLib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace FreqsForbiddenControl
{
    public partial class UserControlFreqsForbidden : UserControl
    {
        /// <summary>
        /// Обновить контрол
        /// </summary>
        /// <param name="listFreqsForbidden"></param>
        public void UpdateFreqsForbidden(List<TableFreqForbidden> listFreqsForbidden)
        {
            try
            {
                if (listFreqsForbidden == null)
                    return;

                ((GlobalFreqsForbidden)DgvFreqsForbidden.DataContext).CollectionFreqsForbidden.Clear();

                for (int i = 0; i < listFreqsForbidden.Count; i++)
                {
                    ((GlobalFreqsForbidden)DgvFreqsForbidden.DataContext).CollectionFreqsForbidden.Add(listFreqsForbidden[i]);
                }

                AddEmptyRows();

            }
            catch { }
        }

        /// <summary>
        /// Добавить несколько записей в контрол
        /// </summary>
        /// <param name="listFreqsForbidden"></param>
        public void AddFreqsForbidden(List<TableFreqForbidden> listFreqsForbidden)
        {
            try
            {
                DeleteEmptyRows();

                for (int i = 0; i < listFreqsForbidden.Count; i++)
                {
                    int ind = ((GlobalFreqsForbidden)DgvFreqsForbidden.DataContext).CollectionFreqsForbidden.ToList().FindIndex(x => x.Id == listFreqsForbidden[i].Id);
                    if (ind != -1)
                    {
                        ((GlobalFreqsForbidden)DgvFreqsForbidden.DataContext).CollectionFreqsForbidden[ind] = listFreqsForbidden[i];
                    }
                    else
                    {
                        ((GlobalFreqsForbidden)DgvFreqsForbidden.DataContext).CollectionFreqsForbidden.Add(listFreqsForbidden[i]);
                    }
                }

                AddEmptyRows();
            }
            catch { }
        }

        /// <summary>
        /// Удалить одну запись из контрола
        /// </summary>
        public void DeleteFreqsForbidden(TableFreqForbidden tableFreqsForbidden)
        {
            try
            {
                int index = ((GlobalFreqsForbidden)DgvFreqsForbidden.DataContext).CollectionFreqsForbidden.ToList().FindIndex(x => x.Id == tableFreqsForbidden.Id);
                ((GlobalFreqsForbidden)DgvFreqsForbidden.DataContext).CollectionFreqsForbidden.RemoveAt(index);

                AddEmptyRows();
            }
            catch { }
        }

        /// <summary>
        /// Удалить все записи из контрола
        /// </summary>
        public void ClearFreqsForbidden()
        {
            try
            {
                ((GlobalFreqsForbidden)DgvFreqsForbidden.DataContext).CollectionFreqsForbidden.Clear();

                AddEmptyRows();
            }
            catch { }
        }

        /// <summary>
        /// Добавить в таблицу пустые строки
        /// </summary>
        private void AddEmptyRows()
        {
            try
            {
                int сountRowsAll = DgvFreqsForbidden.Items.Count; // количество всех строк в таблице
                double hs = 23; // высота строки
                double ah = DgvFreqsForbidden.ActualHeight; // визуализированная высота dataGrid
                double chh = DgvFreqsForbidden.ColumnHeaderHeight; // высота заголовка

                int countRows = Convert.ToInt32(Math.Floor(Convert.ToDouble(ah) - Convert.ToDouble(chh)) / hs); // количество строк, которое помещается в область видимости = (высота таблицы - заголовок таблицы) / высота строки

                int count = сountRowsAll - countRows; // сколько пустых строк нужно удалить
                int index = -1;
                for (int i = 0; i < count; i++)
                {
                    // Удалить пустые строки в dgv
                    index = ((GlobalFreqsForbidden)DgvFreqsForbidden.DataContext).CollectionFreqsForbidden.ToList().FindIndex(x => x.Id < 0);
                    if (index != -1)
                    {
                        ((GlobalFreqsForbidden)DgvFreqsForbidden.DataContext).CollectionFreqsForbidden.RemoveAt(index);
                    }
                }

                List<TableFreqForbidden> list = new List<TableFreqForbidden>();
                for (int i = 0; i < countRows - сountRowsAll; i++)
                {
                    TableFreqForbidden strFR = new TableFreqForbidden
                    {
                        Id = -2,
                        IsActive = false,
                        FreqMinMHz = 0,
                        FreqMaxMHz = 0,
                        Pulse = new ParamDP { Duration = -2F, Period = -2F },
                        Note = string.Empty
                    };
                    list.Add(strFR);
                }

                for (int i = 0; i < list.Count; i++)
                {
                    ((GlobalFreqsForbidden)DgvFreqsForbidden.DataContext).CollectionFreqsForbidden.Add(list[i]);
                }
            }
            catch { }
        }

        /// <summary>
        /// Удалить пустые строки из таблицы
        /// </summary>
        private void DeleteEmptyRows()
        {
            try
            {
                int countEmptyRows = ((GlobalFreqsForbidden)DgvFreqsForbidden.DataContext).CollectionFreqsForbidden.Count(s => s.Id < 0);
                int countAllRows = ((GlobalFreqsForbidden)DgvFreqsForbidden.DataContext).CollectionFreqsForbidden.Count;
                int iCount = countAllRows - countEmptyRows;
                for (int i = iCount; i < countAllRows; i++)
                {
                    ((GlobalFreqsForbidden)DgvFreqsForbidden.DataContext).CollectionFreqsForbidden.RemoveAt(iCount);
                }
            }
            catch { }
        }

        private bool IsSelectedRowEmpty()
        {
            try
            {
                if (((TableFreqForbidden)DgvFreqsForbidden.SelectedItem).Id == -2)
                    return false;
            }
            catch { }

            return true;
        }
    }
}
