﻿using CuirassUModelsDBLib;
using System;
using System.Collections.ObjectModel;
using System.IO;
using System.Windows;
using System.Windows.Controls.WpfPropertyGrid;
using System.Windows.Input;
using System.Windows.Media.Imaging;
using ValuesCorrectLib;

namespace FreqsForbiddenControl
{
    using System.Linq;

    /// <summary>
    /// Логика взаимодействия для FreqRangesProperty.xaml
    /// </summary>
    public partial class FreqsForbiddenProperty : Window
    {
        private ObservableCollection<TableFreqForbidden> collectionTemp;
        public TableFreqForbidden FreqsForbidden { get; private set; }

        public FreqsForbiddenProperty(ObservableCollection<TableFreqForbidden> collectionFreqsForbidden)
        {
            try
            {
                InitializeComponent();

                InitEditors();

                collectionTemp = collectionFreqsForbidden;
                FreqsForbidden = new TableFreqForbidden();
                propertyGrid.SelectedObject = FreqsForbidden;

                //Title = "Add record";
                Title = SMeaning.meaningAddRecord;
                Icon = new BitmapImage(new Uri(@"pack://application:,,,/"
                                    + System.Reflection.Assembly.GetExecutingAssembly().GetName().Name
                                    + ";component/"
                                    + "Resources/AddRec.ico", UriKind.Absolute));

                InitProperty();
                //ChangeCategories();
            }
            catch { }
        }

        public FreqsForbiddenProperty(ObservableCollection<TableFreqForbidden> collectionFreqsForbidden, TableFreqForbidden tableFreqsForbidden)
        {
            try
            {
                InitializeComponent();

                InitEditors();

                collectionTemp = collectionFreqsForbidden;
                FreqsForbidden = tableFreqsForbidden;

                FreqsForbidden.FreqMinMHz = FreqsForbidden.FreqMinMHz ;
                FreqsForbidden.FreqMaxMHz = FreqsForbidden.FreqMaxMHz ;
                propertyGrid.SelectedObject = FreqsForbidden;

                //Title = "Change record";
                Title = SMeaning.meaningChangeRecord;
                Icon = new BitmapImage(new Uri(@"pack://application:,,,/"
                                    + System.Reflection.Assembly.GetExecutingAssembly().GetName().Name
                                    + ";component/"
                                    + "Resources/ChangeRec.ico", UriKind.Absolute));

                //ChangeCategories();
                InitProperty();
            }
            catch { }
        }

        public FreqsForbiddenProperty()
        {
            InitializeComponent();

            InitEditors();
            //ChangeCategories();
            InitProperty();
        }


        private void ButtonApply_Click(object sender, RoutedEventArgs e)
        {
            if (IsAddClick((TableFreqForbidden)propertyGrid.SelectedObject) != null)
            {
                DialogResult = true;
            }
        }

        public TableFreqForbidden IsAddClick(TableFreqForbidden FreqsForbiddenWindow)
        {
            CorrectParams.IsCorrectMinMax(FreqsForbiddenWindow.FreqMinMHz, FreqsForbiddenWindow.FreqMaxMHz);
            if (CorrectParams.IsCorrectFreqMinMax(FreqsForbiddenWindow.FreqMinMHz, FreqsForbiddenWindow.FreqMaxMHz))
            {
                FreqsForbiddenWindow.IsActive = true;
                return FreqsForbiddenWindow;
            }

            return null;
        }

        private void ButtonNoApply_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = false;
        }

        private void InitEditors()
        {
            propertyGrid.Editors.Add(new FreqsForbiddenFminEditor(nameof(FreqsForbidden.FreqMinMHz), typeof(TableFreqForbidden)));
            propertyGrid.Editors.Add(new FreqsForbiddenFmaxEditor(nameof(FreqsForbidden.FreqMaxMHz), typeof(TableFreqForbidden)));
            propertyGrid.Editors.Add(new FreqsForbiddenPulseEditor(nameof(FreqsForbidden.Pulse), typeof(TableFreqForbidden)));
            //propertyGrid.Editors.Add(new FreqsForbiddenTauPulseEditor(nameof(FreqsForbidden.Pulse.Duration), typeof(TableFreqForbidden)));
            //propertyGrid.Editors.Add(new FreqsForbiddenTpulseEditor(nameof(FreqsForbidden.Pulse.Period), typeof(TableFreqForbidden)));
            propertyGrid.Editors.Add(new FreqsForbiddenNoteEditor(nameof(FreqsForbidden.Note), typeof(TableFreqForbidden)));
        }

        private void InitProperty()
        {
            foreach (var property in propertyGrid.Properties)
            {
                try
                {
                    if (property.IsBrowsable == false) { continue; }

                    if (property.PropertyValue.SubProperties.Count != 0)
                    {
                        foreach (var subProperty in property.PropertyValue.SubProperties)
                            subProperty.PropertyValue.PropertyValueException += PropertyGridSubException;
                        continue;
                    }
                    property.PropertyValue.PropertyValueException += PropertyGridException;
                }
                catch (Exception ex)
                { System.Windows.MessageBox.Show(ex.Message); }
            }
        }

        private void PropertyGridSubException(object sender, ValueExceptionEventArgs e)
        {
            propertyGrid.Properties[(sender as PropertyItemValue).ParentProperty.ParentValue.ParentProperty.Name].SetValue((sender as PropertyItemValue).Value);
        }

        private void PropertyGridException(object sender, ValueExceptionEventArgs e)
        {
            propertyGrid.Properties[(sender as PropertyItemValue).ParentProperty.Name].SetValue((sender as PropertyItemValue).Value);
        }

        private void gridProperty_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == System.Windows.Input.Key.Enter)
            {
                if (IsAddClick((TableFreqForbidden)propertyGrid.SelectedObject) != null)
                {
                    DialogResult = true;
                }
            }
        }

        public void SetLanguagePropertyGrid(Languages language)
        {
            LoadTranslatorPropertyGrid(language);
            TranslatorTables.ChangeLanguagePropertyGrid(language, propertyGrid);
        }

        private void LoadTranslatorPropertyGrid(Languages language)
        {
            ResourceDictionary dict = new ResourceDictionary();
            try
            {
                var path = Directory.GetParent(Environment.CurrentDirectory).Parent.Parent.FullName.Split(Path.DirectorySeparatorChar).Last();
                switch (language)
                {
                    case Languages.Eng:
                        dict.Source = new Uri($"/{path};component/Languages/TranslatorTables/TranslatorTablesCuirasse.EN.xaml",
                            UriKind.Relative);
                        break;
                    case Languages.Rus:

                        dict.Source = new Uri($"/{path};component/Languages/TranslatorTables/TranslatorTablesCuirasse.RU.xaml",
                            UriKind.Relative);
                        break;
                    default:
                        dict.Source = new Uri($"/{path};component/Languages/TranslatorTables/TranslatorTablesCuirasse.RU.xaml",
                            UriKind.Relative);
                        break;
                }
             
                this.Resources.MergedDictionaries.Add(dict);
            }
            catch (Exception ex)
            {
                //MessageBox.Show(ex.Message);
            }
        }
    }
}
