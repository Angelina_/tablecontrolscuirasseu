﻿using CuirassUModelsDBLib;
using System;
using System.Collections.ObjectModel;
using System.Windows;

namespace FreqsForbiddenControl
{
    public class GlobalFreqsForbidden
    {
        public ObservableCollection<TableFreqForbidden> CollectionFreqsForbidden { get; set; }

        public GlobalFreqsForbidden()
        {
            try
            {
                CollectionFreqsForbidden = new ObservableCollection<TableFreqForbidden> { };

                //CollectionFreqsForbidden = new ObservableCollection<TableFreqForbidden>
                //{
                //    new TableFreqForbidden
                //    {
                //        Id = 1,
                //        FreqMinMHz = 565999.98,
                //        FreqMaxMHz = 1785900.88,
                //        Pulse = new ParamDP{ Duration = 150.6F, Period = 0.34340F },
                //        Note = "Jk8 dflk okl"
                //    },
                //    new TableFreqForbidden
                //    {
                //        Id = 2,
                //        FreqMinMHz = 455999.58,
                //        FreqMaxMHz = 788900.78,
                //        Pulse = new ParamDP{ Duration = 10550.6F, Period = 0.233430F },
                //        Note = "jkdsgj 878"
                //    }
                //};
            }
            catch (Exception ex) { MessageBox.Show(ex.Message); }
        }
    }
}
