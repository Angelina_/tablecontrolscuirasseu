﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Media;
using GrpcDbClientLib;
using CuirassUModelsDBLib;
using CuirassUModelsDBLib.InheritorsEventArgs;
using System.Threading;
using System.Threading.Tasks;

namespace ProjectTablesControlTEST
{
    public partial class MainWindow : Window
    {
        public void DispatchIfNecessary(Action action)
        {
            if (!Dispatcher.CheckAccess())
                Dispatcher.Invoke(action);
            else
                action.Invoke();
        }

        private void DbConnection_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (serviceClient != null)
                    serviceClient.Disconnect();
                else
                {
                    serviceClient = new ServiceClient(this.Name, ip, port);
                    InitClientDB();
                    serviceClient.Connect();
                }
            }
            catch (ExceptionClient exceptClient)
            {

            }
        }

        private void ClientDB_OnConnect(object sender, ClientEventArgs e)
        {
            DispatchIfNecessary(() =>
            {
                DbControlConnection.ShowConnect();
                LoadTables();
            });
        }

        void HandlerDisconnect(object obj, ClientEventArgs eventArgs)
        {
            if (eventArgs.GetMessage != "")
            {
                DbControlConnection.ShowDisconnect();
            }
            serviceClient = null;
        }

        void HandlerUpData(object obj, DataEventArgs eventArgs)
        {
           
        }
       
    }
}
