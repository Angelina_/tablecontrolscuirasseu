﻿using CuirassUModelsDBLib;
using CuirassUModelsDBLib.InheritorsEventArgs;
using GrpcDbClientLib;
using System;
using System.Linq;
using System.Threading;
using System.Windows;
using TableEvents;

namespace ProjectTablesControlTEST
{
    public partial class MainWindow
    {
        private void UcRES_OnMetering(object sender, TableEvent e)
        {
            int i = 10;
        }

        private void OnUpTable_TableRES(object sender, TableEventArgs<TableRes> e)
        {
            Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal, (ThreadStart)delegate ()
            {
                lRES = e.Table;
                if (lRES.Count == 0) { ucRES.UpdateRES(lRES); }
                else { ucRES.AddRES(lRES); }
            });
        }

        private void UcRES_OnSelectedRow(object sender, SelectedRowEvents e)
        {
            if (e.Id > -1)
            {
               
            }
        }

        private void OnUpTable_TableFreqRangesRecon(object sender, TableEventArgs<TableFreqRangesRecon> e)
        {
            Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal, (ThreadStart)delegate ()
            {
                lFreqRangesRecon = (from t in e.Table let a = t as TableFreqRanges select a).ToList();
                ucFreqRangesRecon.UpdateFreqRanges(lFreqRangesRecon);
            });
        }

        private void OnUpTable_TableFreqForbidden(object sender, TableEventArgs<TableFreqForbidden> e)
        {
            Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal, (ThreadStart)delegate ()
            {
                lFreqForbidden = e.Table;
                ucFreqsForbidden.UpdateFreqsForbidden(lFreqForbidden);
            });
        }

        private void OnUpTable_TableSectorsRecon(object sender, TableEventArgs<TableSectorsRecon> e)
        {
            Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal, (ThreadStart)delegate ()
            {
                lSectorsRecon = e.Table;
                ucSectorsRecon.UpdateSectorsRecon(lSectorsRecon);
            });
        }

        private void OnUpTable_TableRemotePoints(object sender, TableEventArgs<TableRemotePoints> e)
        {
            Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal, (ThreadStart)delegate ()
            {
                lRemotePoints = e.Table;
                ucRemotePoints.UpdateRemotePoints(lRemotePoints); 
            });
        }

        private void OnUpTable_TableResDistribution(object sender, TableEventArgs<TableResDistribution> e)
        {
            Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal, (ThreadStart)delegate ()
            {
                lResDistribution = e.Table;
                ucRESDistribution.ListRESDistribution = lResDistribution;
            });
        }

        private void OnUpTable_TableResArchive(object sender, TableEventArgs<TableResArchive> e)
        {
            Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal, (ThreadStart)delegate ()
            {
                lResArchive = e.Table;
                ucRESArchive.ListRESArchive = lResArchive;
            });
        }

        private void OnUpTable_TableResPattern(object sender, TableEventArgs<TableResPattern> e)
        {
            this.Dispatcher.BeginInvoke(
                System.Windows.Threading.DispatcherPriority.Normal,
                (ThreadStart)delegate()
                    {
                        this.lResPattern = e.Table;
                this.ucRESPattern.UpdateRESPattern(lResPattern);
            });
        }

        private async void LoadTables()
        {
            try
            {
                lFreqForbidden = await serviceClient.Tables[NameTable.TableFreqForbidden].LoadAsync<TableFreqForbidden>();
                ucFreqsForbidden.UpdateFreqsForbidden(lFreqForbidden);

                lFreqRangesRecon = await serviceClient.Tables[NameTable.TableFreqRangesRecon].LoadAsync<TableFreqRanges>();
                ucFreqRangesRecon.UpdateFreqRanges(lFreqRangesRecon);

                lSectorsRecon = await serviceClient.Tables[NameTable.TableSectorsRecon].LoadAsync<TableSectorsRecon>();
                ucSectorsRecon.UpdateSectorsRecon(lSectorsRecon);
            }
            catch (ExceptionClient exeptClient)
            {
                MessageBox.Show(exeptClient.Message);
            }
            catch (ExceptionDatabase excpetService)
            {
                MessageBox.Show(excpetService.Message);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}
