﻿using CuirassUModelsDBLib;
using CuirassUModelsDBLib.InheritorsEventArgs;
using GrpcDbClientLib;
using System;

namespace ProjectTablesControlTEST
{
    public partial class MainWindow
    {
        ServiceClient serviceClient;
        string ip = "127.0.0.1";
        int port = 30051;

        void InitClientDB()
        {
            serviceClient.OnConnect += ClientDB_OnConnect;
            serviceClient.OnDisconnect += HandlerDisconnect;
            serviceClient.OnUpData += HandlerUpData;

            (serviceClient.Tables[NameTable.TableRes] as ITableUpdate<TableRes>).OnUpTable += OnUpTable_TableRES;
            (serviceClient.Tables[NameTable.TableFreqRangesRecon] as ITableUpdate<TableFreqRangesRecon>).OnUpTable += OnUpTable_TableFreqRangesRecon;
            (serviceClient.Tables[NameTable.TableFreqForbidden] as ITableUpdate<TableFreqForbidden>).OnUpTable += OnUpTable_TableFreqForbidden;
            (serviceClient.Tables[NameTable.TableSectorsRecon] as ITableUpdate<TableSectorsRecon>).OnUpTable += OnUpTable_TableSectorsRecon;
            (serviceClient.Tables[NameTable.TableRemotePoints] as ITableUpdate<TableRemotePoints>).OnUpTable += OnUpTable_TableRemotePoints;
            (serviceClient.Tables[NameTable.TableResDistribution] as ITableUpdate<TableResDistribution>).OnUpTable += OnUpTable_TableResDistribution;
            (serviceClient.Tables[NameTable.TableResPattern] as ITableUpdate<TableResPattern>).OnUpTable += OnUpTable_TableResPattern;
            (serviceClient.Tables[NameTable.TableResArchive] as ITableUpdate<TableResArchive>).OnUpTable += OnUpTable_TableResArchive;
        }

   
    }
}
