﻿using CuirassUModelsDBLib;
using System;
using System.ComponentModel;
using System.Windows;
using ValuesCorrectLib;

namespace ProjectTablesControlTEST
{
    using System.IO;
    using System.Linq;

    public partial class MainWindow : Window, INotifyPropertyChanged
    {

        private void SetLanguageTables(Languages language)
        {
            ResourceDictionary dict = new ResourceDictionary();
            try
            {
                var path = Directory.GetParent(Environment.CurrentDirectory).Parent.Parent.FullName.Split(Path.DirectorySeparatorChar).Last();
                switch (language)
                {
                    case Languages.Eng:
                        dict.Source = new Uri($"/{path};component/Languages/TranslatorTables/TranslatorTablesCuirasse.EN.xaml",
                            UriKind.Relative);
                        break;
                    case Languages.Rus:

                        dict.Source = new Uri($"/{path};component/Languages/TranslatorTables/TranslatorTablesCuirasse.RU.xaml",
                            UriKind.Relative);
                        break;
                    default:
                        dict.Source = new Uri($"/{path};component/Languages/TranslatorTables/TranslatorTablesCuirasse.RU.xaml",
                            UriKind.Relative);
                        break;
                }

                this.Resources.MergedDictionaries.Add(dict);
            }
            catch (Exception ex)
            { }
        }

        private void basicProperties_OnLanguageChanged(object sender, Languages language)
        {
            SetLanguageTables(language);
            TranslatorTables.LoadDictionary(language);
        }

       
    }
}
