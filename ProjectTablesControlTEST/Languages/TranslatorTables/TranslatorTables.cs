﻿using CuirassUModelsDBLib;
using System.ComponentModel;
using System.Windows;

namespace ProjectTablesControlTEST
{
    public partial class MainWindow : Window, INotifyPropertyChanged
    {

        private void UcFreqRangesRecon_OnIsWindowPropertyOpen(object sender, FreqRangesControl.FreqRangesProperty e)
        {
            e.SetLanguagePropertyGrid(Languages.Rus);
        }

        //private void UcFreqForbidden_OnIsWindowPropertyOpen(object sender, FreqRangesControl.FreqRangesProperty e)
        //{
        //    e.SetLanguagePropertyGrid(basicProperties.Local.Common.Language);
        //}

        private void UcSectorsRecon_OnIsWindowPropertyOpen(object sender, SectorsReconControl.SectorsReconProperty e)
        {
            e.SetLanguagePropertyGrid(Languages.Rus);
        }

        private void UcFreqsForbidden_OnIsWindowPropertyOpen(object sender, FreqsForbiddenControl.FreqsForbiddenProperty e)
        {
            e.SetLanguagePropertyGrid(Languages.Rus);
        }

        private void UcRemotePoints_OnIsWindowPropertyOpen(object sender, RemotePointsControl.RemotePointsProperty e)
        {
            e.SetLanguagePropertyGrid(Languages.Rus);
        }

        private void UcRESPattern_OnIsWindowPropertyOpen(object sender, RESPatternControl.RESPatternProperty e)
        {
            e.SetLanguagePropertyGrid(Languages.Rus);
        }
    }
}
