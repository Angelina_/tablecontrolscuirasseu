﻿using CuirassUModelsDBLib;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Windows;
using ValuesCorrectLib;

namespace ProjectTablesControlTEST
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window, INotifyPropertyChanged
    {
        #region INotifyPropertyChanged
        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged([CallerMemberName]string prop = "")
        {
            try
            {
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));
            }
            catch { }
        }
        #endregion

        public MainWindow()
        {
            InitializeComponent();

            this.Name = "TestClient";

            InitTables();

            SetLanguageTables(Languages.Rus);

            TranslatorTables.LoadDictionary(Languages.Rus);
        }

     
    }
}
