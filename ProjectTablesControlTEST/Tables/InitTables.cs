﻿using CuirassUModelsDBLib;
using System;
using TableEvents;

namespace ProjectTablesControlTEST
{
    public partial class MainWindow
    {
        public void InitTables()
        {
            // Таблица ИРИ
            ucRES.OnDeleteRecord += new EventHandler<TableEvent>(OnDeleteRecord);
            ucRES.OnClearRecords += new EventHandler<NameTable>(OnClearRecords);
            ucRES.OnSelectedRow += new EventHandler<SelectedRowEvents>(UcRES_OnSelectedRow);
            ucRES.OnMetering += new EventHandler<TableEvent>(UcRES_OnMetering);

            // Таблица Запрещенные частоты (ИЧ)
            //ucFreqForbidden.OnAddRecord += new EventHandler<TableEvent>(OnAddRecord);
            //ucFreqForbidden.OnChangeRecord += new EventHandler<TableEvent>(OnChangeRecord);
            //ucFreqForbidden.OnDeleteRecord += new EventHandler<TableEvent>(OnDeleteRecord);
            //ucFreqForbidden.OnClearRecords += new EventHandler<NameTable>(OnClearRecords);
            //ucFreqForbidden.OnIsWindowPropertyOpen += new EventHandler<FreqRangesControl.FreqRangesProperty>( UcFreqForbidden_OnIsWindowPropertyOpen);

            // Таблица Диапазоны радиоразведки (ДРР)
            ucFreqRangesRecon.OnAddRecord += new EventHandler<TableEvent>(OnAddRecord);
            ucFreqRangesRecon.OnChangeRecord += new EventHandler<TableEvent>(OnChangeRecord);
            ucFreqRangesRecon.OnDeleteRecord += new EventHandler<TableEvent>(OnDeleteRecord);
            ucFreqRangesRecon.OnClearRecords += new EventHandler<NameTable>(OnClearRecords);
            ucFreqRangesRecon.OnIsWindowPropertyOpen += new EventHandler<FreqRangesControl.FreqRangesProperty>(UcFreqRangesRecon_OnIsWindowPropertyOpen);

            // Таблица Сектора радиоразведки (СРР)
            ucSectorsRecon.OnAddRecord += new EventHandler<TableEvent>(OnAddRecord);
            ucSectorsRecon.OnChangeRecord += new EventHandler<TableEvent>(OnChangeRecord);
            ucSectorsRecon.OnDeleteRecord += new EventHandler<TableEvent>(OnDeleteRecord);
            ucSectorsRecon.OnClearRecords += new EventHandler<NameTable>(OnClearRecords);
            ucSectorsRecon.OnIsWindowPropertyOpen += new EventHandler<SectorsReconControl.SectorsReconProperty>(UcSectorsRecon_OnIsWindowPropertyOpen);

            // Таблица Запрещенные частоты (ИЧ)
            ucFreqsForbidden.OnAddRecord += new EventHandler<TableEvent>(OnAddRecord);
            ucFreqsForbidden.OnChangeRecord += new EventHandler<TableEvent>(OnChangeRecord);
            ucFreqsForbidden.OnDeleteRecord += new EventHandler<TableEvent>(OnDeleteRecord);
            ucFreqsForbidden.OnClearRecords += new EventHandler<NameTable>(OnClearRecords);
            ucFreqsForbidden.OnIsWindowPropertyOpen += new EventHandler<FreqsForbiddenControl.FreqsForbiddenProperty>(UcFreqsForbidden_OnIsWindowPropertyOpen);

            // Таблица Удаленные пункты приема (УПП)
            ucRemotePoints.OnAddRecord += new EventHandler<TableEvent>(OnAddRecord);
            ucRemotePoints.OnChangeRecord += new EventHandler<TableEvent>(OnChangeRecord);
            ucRemotePoints.OnDeleteRecord += new EventHandler<TableEvent>(OnDeleteRecord);
            ucRemotePoints.OnClearRecords += new EventHandler<NameTable>(OnClearRecords);
            ucRemotePoints.OnIsWindowPropertyOpen += new EventHandler<RemotePointsControl.RemotePointsProperty>(UcRemotePoints_OnIsWindowPropertyOpen);

            // Таблица ИРИ ЦР
            ucRESDistribution.OnDeleteRecord += new EventHandler<TableEvent>(OnDeleteRecord);
            ucRESDistribution.OnClearRecords += new EventHandler<NameTable>(OnClearRecords);

            // Таблица Шаблоны ИРИ
            ucRESPattern.OnAddRecord += new EventHandler<TableEvent>(OnAddRecord);
            ucRESPattern.OnChangeRecord += new EventHandler<TableEvent>(OnChangeRecord);
            ucRESPattern.OnDeleteRecord += new EventHandler<TableEvent>(OnDeleteRecord);
            ucRESPattern.OnClearRecords += new EventHandler<NameTable>(OnClearRecords);
            ucRESPattern.OnIsWindowPropertyOpen += new EventHandler<RESPatternControl.RESPatternProperty>(UcRESPattern_OnIsWindowPropertyOpen);
        }

      
    }
}
