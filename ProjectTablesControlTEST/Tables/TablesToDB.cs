﻿using CuirassUModelsDBLib;
using System.Collections.Generic;
using TableEvents;

namespace ProjectTablesControlTEST
{
    public partial class MainWindow
    {
        // ИРИ
        public List<TableRes> lRES = new List<TableRes>();
        // Известные частоты (ИЧ)
        public List<TableFreqForbidden> lFreqForbidden = new List<TableFreqForbidden>();
        // Диапазоны радиоразведки (ДРР)
        public List<TableFreqRanges> lFreqRangesRecon = new List<TableFreqRanges>();
        // Сектора радиоразведки (СРР)
        public List<TableSectorsRecon> lSectorsRecon = new List<TableSectorsRecon>();
        // Удаленные пункты приема (УПП)
        public List<TableRemotePoints> lRemotePoints = new List<TableRemotePoints>();
        // Удаленные ИРИ ЦР
        public List<TableResDistribution> lResDistribution = new List<TableResDistribution>();
        // Шаблоны ИРИ
        public List<TableResPattern> lResPattern = new List<TableResPattern>();
        // ИРИ Архив
        public List<TableResArchive> lResArchive = new List<TableResArchive>();

        // Добавить запись
        private void OnAddRecord(object sender, TableEvent e)
        {
            if (serviceClient != null)
            {
                serviceClient.Tables[e.NameTable].Add(e.Record);
            }
        }

        // Удалить все записи
        private void OnClearRecords(object sender, NameTable nameTable)
        {
            if (serviceClient != null)
            {
                serviceClient.Tables[nameTable].Clear();
            }
        }

        // Удалить запись
        private void OnDeleteRecord(object sender, TableEvent e)
        {
            if (serviceClient != null)
            {
                serviceClient.Tables[e.NameTable].Delete(e.Record);
            }
        }

        // Изменить запись
        private void OnChangeRecord(object sender, TableEvent e)
        {
            if (serviceClient != null)
            {
                serviceClient.Tables[e.NameTable].Change(e.Record);
            }
        }
    }
}
