﻿using CuirassUModelsDBLib;
using System;
using System.Collections.ObjectModel;
using System.Windows;

namespace RESArchiveControl
{
    public class GlobalRESArchive
    {
        public ObservableCollection<TableResArchive> CollectionRESArchive { get; set; }

        public GlobalRESArchive()
        {
            try
            {
                CollectionRESArchive = new ObservableCollection<TableResArchive> { };

                //CollectionRESArchive = new ObservableCollection<TableResArchive>
                //{
                //    new TableResArchive
                //    {
                //        Id = 1,
                //        FrequencyKHz = 569599.98,
                //        Pulse = new ParamDP { Duration = 10550.6F, Period = 0.233430F },
                //        Series = new ParamDP { Duration = 340.887F, Period = 45.34343F },
                //        TargetType = "QQQ",
                //        AntennaMaster = (byte)1,
                //        AntennaSlave = (byte)3,
                //        DateTimeStart = DateTime.Now,
                //        DateTimeStop = DateTime.Now,
                //        Track = new ObservableCollection<TableTrackArchive>
                //        {
                //            new TableTrackArchive
                //            {
                //                Id = 1,
                //                Coordinates = new Coord { Latitude = 53.76566655, Longitude = 27.4545353, Altitude = 250 }
                //            },
                //            new TableTrackArchive
                //            {
                //                Id = 2,
                //                Coordinates = new Coord { Latitude = 51.522555, Longitude = -32.11115353, Altitude = 478 }
                //            },
                //            new TableTrackArchive
                //            {
                //                Id = 3,
                //                Coordinates = new Coord { Latitude = 57.522555, Longitude = -31.11115353, Altitude = 478 }
                //            }
                //        },
                //        Note = "Jk8 dflk okl"
                //    },
                //    new TableResArchive
                //    {
                //        Id = 2,
                //        FrequencyKHz = 345666.4668,
                //        Pulse = new ParamDP { Duration = 200.1F, Period = 420.77F },
                //        Series = new ParamDP { Duration = 112.33F, Period = 56.5F },
                //        TargetType = "54 bjg",
                //        AntennaMaster = (byte)2,
                //        AntennaSlave = (byte)4,
                //        DateTimeStart = DateTime.Now,
                //        DateTimeStop = DateTime.Now,
                //        Track = new ObservableCollection<TableTrackArchive>
                //        {
                //            new TableTrackArchive
                //            {
                //                Id = 1,
                //                Coordinates = new Coord { Latitude = 54.522555, Longitude = -30.11115353, Altitude = 478 }
                //            },
                //            new TableTrackArchive
                //            {
                //                Id = 2,
                //                Coordinates = new Coord { Latitude = 57.522555, Longitude = -24.11115353, Altitude = 478 }
                //            }
                //        },
                //        Note = "jkdsgj 878"
                //    }
                //};
            }
            catch (Exception ex) { MessageBox.Show(ex.Message); }
        }
    }
}
