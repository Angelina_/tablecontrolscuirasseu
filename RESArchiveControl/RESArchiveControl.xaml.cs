﻿using CuirassUModelsDBLib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using TableEvents;

namespace RESArchiveControl
{
    /// <summary>
    /// Логика взаимодействия для UserControl1.xaml
    /// </summary>
    public partial class UserControlRESArchive : UserControl
    {
        #region Events
        public event EventHandler<TableEvent> OnDeleteRecord = (object sender, TableEvent data) => { };
        public event EventHandler<NameTable> OnClearRecords = (object sender, NameTable data) => { };
        #endregion

        public UserControlRESArchive()
        {
            InitializeComponent();

            DgvRESArchive.DataContext = new GlobalRESArchive();
        }

        private void ButtonDelete_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if ((TableResArchive)DgvRESArchive.SelectedItem != null)
                {
                    if (!IsSelectedRowEmpty())
                        return;

                    TableResArchive tableRESArchive = new TableResArchive
                    {
                        Id = ((TableResArchive)DgvRESArchive.SelectedItem).Id
                    };

                    // Событие удаления одной записи
                    OnDeleteRecord(this, new TableEvent(tableRESArchive));
                }
            }
            catch { }
        }

        private void ButtonClear_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                // Событие удаления записей
                OnClearRecords(this, NameTable.TableResArchive);
            }
            catch { }
        }

        private void DgvRESArchive_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            AddEmptyRows();
        }

        private void DgvRESArchive_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if ((TableResArchive)DgvRESArchive.SelectedItem == null) return;

            if (((TableResArchive)DgvRESArchive.SelectedItem).Id > -1)
            {
                if (((TableResArchive)DgvRESArchive.SelectedItem).Id != PropNumRESArchive.SelectedNumRESArchive)
                {
                    PropNumRESArchive.SelectedNumRESArchive = ((TableResArchive)DgvRESArchive.SelectedItem).Id;
                    PropNumRESArchive.IsSelectedNumRESArchive = true;

                    //OnSelectedRow(this, new SelectedRowEvents(PropNumRESDistribution.SelectedNumRESDistribution));
                }
            }
            else
            {
                PropNumRESArchive.SelectedNumRESArchive = 0;
                PropNumRESArchive.IsSelectedNumRESArchive = false;
            }
        }
    }
}
