﻿using CuirassUModelsDBLib;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using TableEvents;

namespace RESArchiveControl
{
    public partial class UserControlRESArchive : UserControl
    {
        /// <summary>
        /// Обновить контрол
        /// </summary>
        /// <param name="listRESDistribution"></param>
        private List<TableResArchive> listRESArchive = new List<TableResArchive> { };
        public List<TableResArchive> ListRESArchive
        {
            get { return listRESArchive; }
            set
            {
                if (listRESArchive != null && listRESArchive.Equals(value)) return;
                listRESArchive = value;
                UpdateRESArchive();
            }
        }

        private void UpdateRESArchive()
        {
            try
            {
                DeleteEmptyRows();

                if (ListRESArchive == null)
                    return;

                ((GlobalRESArchive)DgvRESArchive.DataContext).CollectionRESArchive.Clear();

                for (int i = 0; i < ListRESArchive.Count; i++)
                {
                    ((GlobalRESArchive)DgvRESArchive.DataContext).CollectionRESArchive.Add(ListRESArchive[i]);
                }

                AddEmptyRows();

                DgvRESArchive.SelectedIndex = PropNumRESArchive.SelectedNumRESArchive;

            }
            catch { }
        }

        /// <summary>
        /// Добавить несколько записей в контрол
        /// </summary>
        /// <param name="listRESArchive"></param>
        public void AddRESArchive(List<TableResArchive> listRESArchive)
        {
            try
            {
                DeleteEmptyRows();

                for (int i = 0; i < listRESArchive.Count; i++)
                {
                    int ind = ((GlobalRESArchive)DgvRESArchive.DataContext).CollectionRESArchive.ToList().FindIndex(x => x.Id == listRESArchive[i].Id);
                    if (ind != -1)
                    {
                        ((GlobalRESArchive)DgvRESArchive.DataContext).CollectionRESArchive[ind] = listRESArchive[i];
                    }
                    else
                    {
                        ((GlobalRESArchive)DgvRESArchive.DataContext).CollectionRESArchive.Add(listRESArchive[i]);
                    }
                }

                //AddEmptyRows();

                DgvRESArchive.SelectedIndex = PropNumRESArchive.SelectedNumRESArchive;
            }
            catch { }
        }

        /// <summary>
        /// Удалить одну запись из контрола
        /// </summary>
        public void DeleteRESArchive(TableResArchive tableRESArchive)
        {
            try
            {
                int index = ((GlobalRESArchive)DgvRESArchive.DataContext).CollectionRESArchive.ToList().FindIndex(x => x.Id == tableRESArchive.Id);
                ((GlobalRESArchive)DgvRESArchive.DataContext).CollectionRESArchive.RemoveAt(index);

                AddEmptyRows();
            }
            catch { }
        }

        /// <summary>
        /// Удалить все записи из контрола
        /// </summary>
        public void ClearRESArchive()
        {
            try
            {
                ((GlobalRESArchive)DgvRESArchive.DataContext).CollectionRESArchive.Clear();

                AddEmptyRows();
            }
            catch { }
        }

        /// <summary>
        /// Добавить в таблицу пустые строки
        /// </summary>
        private void AddEmptyRows()
        {
            try
            {
                int сountRowsAll = DgvRESArchive.Items.Count; // количество всех строк в таблице
                double hs = 23; // высота строки
                double ah = DgvRESArchive.ActualHeight; // визуализированная высота dataGrid
                double chh = DgvRESArchive.ColumnHeaderHeight; // высота заголовка

                int countRows = Convert.ToInt32(Math.Floor(Convert.ToDouble(ah) - Convert.ToDouble(chh)) / hs); // количество строк, которое помещается в область видимости = (высота таблицы - заголовок таблицы) / высота строки

                int count = сountRowsAll - countRows; // сколько пустых строк нужно удалить
                int index = -1;
                for (int i = 0; i < count; i++)
                {
                    // Удалить пустые строки в dgv
                    index = ((GlobalRESArchive)DgvRESArchive.DataContext).CollectionRESArchive.ToList().FindIndex(x => x.Id < 0);
                    if (index != -1)
                    {
                        ((GlobalRESArchive)DgvRESArchive.DataContext).CollectionRESArchive.RemoveAt(index);
                    }
                }

                List<TableResArchive> list = new List<TableResArchive>();
                for (int i = 0; i < countRows - сountRowsAll; i++)
                {
                    TableResArchive strRESArchive = new TableResArchive
                    {
                        Id = -2,
                        FrequencyMHz = 0,
                        Pulse = new ParamDP { Duration = -2F, Period = -2F },
                        Series = new ParamDP { Duration = -2F, Period = -2F },
                        TargetType = string.Empty,
                        AntennaMaster = (byte)255,
                        AntennaSlave = (byte)255,
                        Track = new ObservableCollection<TableTrackArchive>
                        {
                            new TableTrackArchive
                            {
                                Coordinates = new Coord { Latitude = -2, Longitude = -2, Altitude = -2 }
                            }
                        },
                        Note = string.Empty
                    };
                    list.Add(strRESArchive);
                }

                for (int i = 0; i < list.Count; i++)
                {
                    ((GlobalRESArchive)DgvRESArchive.DataContext).CollectionRESArchive.Add(list[i]);
                }
            }
            catch { }
        }

        /// <summary>
        /// Удалить пустые строки из таблицы
        /// </summary>
        private void DeleteEmptyRows()
        {
            try
            {
                int countEmptyRows = ((GlobalRESArchive)DgvRESArchive.DataContext).CollectionRESArchive.Count(s => s.Id < 0);
                int countAllRows = ((GlobalRESArchive)DgvRESArchive.DataContext).CollectionRESArchive.Count;
                int iCount = countAllRows - countEmptyRows;
                for (int i = iCount; i < countAllRows; i++)
                {
                    ((GlobalRESArchive)DgvRESArchive.DataContext).CollectionRESArchive.RemoveAt(iCount);
                }
            }
            catch { }
        }

        private bool IsSelectedRowEmpty()
        {
            try
            {
                if (((TableResArchive)DgvRESArchive.SelectedItem).Id == -2)
                    return false;
            }
            catch { }

            return true;
        }
    }
}
