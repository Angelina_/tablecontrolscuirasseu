﻿using CuirassUModelsDBLib;
using System;
using System.Collections.ObjectModel;
using System.Windows;

namespace RESControl
{
    public class GlobalRES
    {
        public ObservableCollection<TableRes> CollectionRES { get; set; }

        public GlobalRES()
        {
            try
            {
                CollectionRES = new ObservableCollection<TableRes> { };

                //CollectionRES = new ObservableCollection<TableRes>
                //{
                //    new TableRes
                //    {
                //        Id = 1,
                //        FrequencyKHz = 569599.98,
                //        Bearing = 245.7878F,
                //        StandardDeviation = 100.8F,
                //        Pulse = new ParamDP{ Duration = 10550.6F, Period = 0.233430F },
                //        Series = new ParamDP{ Duration = 340.887F, Period = 45.34343F },
                //        Note = "Jk8 dflk okl",
                //        AntennaMaster = (byte)1,
                //        AntennaSlave = (byte)3,
                //        Device = (byte)1

                //    },
                //    new TableRes
                //    {
                //        Id = 2,
                //        FrequencyKHz = 345666.4668,
                //        Bearing = 55.65578F,
                //        StandardDeviation = 40.6F,
                //        Pulse = new ParamDP{ Duration = 200.1F, Period = 420.77F },
                //        Series = new ParamDP{ Duration = 112.33F, Period = 56.5F },
                //        Note = "jkdsgj 878",
                //        AntennaMaster = (byte)2,
                //        AntennaSlave = (byte)4,
                //        Device = (byte)2
                //    }
                //};
            }
            catch (Exception ex) { MessageBox.Show(ex.Message); }
        }
    }
}
