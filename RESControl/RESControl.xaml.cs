﻿using CuirassUModelsDBLib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using TableEvents;

namespace RESControl
{
    /// <summary>
    /// Логика взаимодействия для UserControl1.xaml
    /// </summary>
    public partial class UserControlRES : UserControl
    {
        #region Events
        public event EventHandler<TableEvent> OnDeleteRecord = (object sender, TableEvent data) => { };
        public event EventHandler<NameTable> OnClearRecords = (object sender, NameTable data) => { };
        public event EventHandler<TableEvent> OnMetering = (object sender, TableEvent data) => { };

        public event EventHandler<SelectedRowEvents> OnSelectedRow = (object sender, SelectedRowEvents data) => { };

        public event EventHandler<TableEventReport> OnAddTableToReport = (object sender, TableEventReport data) => { };
        #endregion

        #region Properties
        public NameTable NameTable { get; set; } = NameTable.TableRes;
        #endregion
        public UserControlRES()
        {
            InitializeComponent();

            DgvRES.DataContext = new GlobalRES();
        }

        private void ButtonDelete_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if ((TableRes)DgvRES.SelectedItem != null)
                {
                    if (!IsSelectedRowEmpty())
                        return;

                    TableRes tableRES = new TableRes
                    {
                        Id = ((TableRes)DgvRES.SelectedItem).Id
                    };

                    // Событие удаления одной записи
                    OnDeleteRecord(this, new TableEvent(tableRES));
                }
            }
            catch { }
        }

        private void ButtonClear_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                // Событие удаления записей
                OnClearRecords(this, NameTable.TableRes);
            }
            catch { }
        } 
        
        private void DgvRES_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            AddEmptyRows();
        }

        private void DgvRES_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if ((TableRes)DgvRES.SelectedItem == null) return;

            if (((TableRes)DgvRES.SelectedItem).Id > -1)
            {
                if (((TableRes)DgvRES.SelectedItem).Id != PropNumRES.SelectedNumRES)
                {
                    PropNumRES.SelectedNumRES = ((TableRes)DgvRES.SelectedItem).Id;
                    PropNumRES.IsSelectedNumRES = true;

                    OnSelectedRow(this, new SelectedRowEvents(PropNumRES.SelectedNumRES));
                }
            }
            else
            {
                PropNumRES.SelectedNumRES = 0;
                PropNumRES.IsSelectedNumRES = false;
            }
        }

        private void bMetering_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if ((TableRes)DgvRES.SelectedItem != null)
                {
                    if (!IsSelectedRowEmpty())
                        return;

                    // Событие Передать объект на измерение
                    OnMetering(this, new TableEvent((TableRes)DgvRES.SelectedItem));
                }
            }
            catch { }
        }
    }
}
