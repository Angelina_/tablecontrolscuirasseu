﻿using CuirassUModelsDBLib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using TableEvents;

namespace RESControl
{
    public partial class UserControlRES : UserControl
    {
        /// <summary>
        /// Обновить контрол
        /// </summary>
        /// <param name="listRES"></param>
        public void UpdateRES(List<TableRes> listRES)
        {
            try
            {
                DeleteEmptyRows();
                if (listRES == null)
                    return;

                ((GlobalRES)DgvRES.DataContext).CollectionRES.Clear();

                for (int i = 0; i < listRES.Count; i++)
                {
                    ((GlobalRES)DgvRES.DataContext).CollectionRES.Add(listRES[i]);
                }

                //AddEmptyRows();

            }
            catch { }
        }

        /// <summary>
        /// Добавить несколько записей в контрол
        /// </summary>
        /// <param name="listRES"></param>
        public void AddRES(List<TableRes> listRES)
        {
            try
            {
                DeleteEmptyRows();

                for (int i = 0; i < listRES.Count; i++)
                {
                    int ind = ((GlobalRES)DgvRES.DataContext).CollectionRES.ToList().FindIndex(x => x.Id == listRES[i].Id);
                    if (ind != -1)
                    {
                        ((GlobalRES)DgvRES.DataContext).CollectionRES[ind] = listRES[i];
                    }
                    else
                    {
                        ((GlobalRES)DgvRES.DataContext).CollectionRES.Add(listRES[i]);
                    }
                }

                //AddEmptyRows();

                DgvRES.SelectedIndex = PropNumRES.SelectedNumRES;
            }
            catch { }
        }

        /// <summary>
        /// Удалить одну запись из контрола
        /// </summary>
        public void DeleteRES(TableRes tableRES)
        {
            try
            {
                int index = ((GlobalRES)DgvRES.DataContext).CollectionRES.ToList().FindIndex(x => x.Id == tableRES.Id);
                ((GlobalRES)DgvRES.DataContext).CollectionRES.RemoveAt(index);

                AddEmptyRows();
            }
            catch { }
        }

        /// <summary>
        /// Удалить все записи из контрола
        /// </summary>
        public void ClearRES()
        {
            try
            {
                ((GlobalRES)DgvRES.DataContext).CollectionRES.Clear();

                AddEmptyRows();
            }
            catch { }
        }

        /// <summary>
        /// Добавить в таблицу пустые строки
        /// </summary>
        private void AddEmptyRows()
        {
            try
            {
                int сountRowsAll = DgvRES.Items.Count; // количество всех строк в таблице
                double hs = 23; // высота строки
                double ah = DgvRES.ActualHeight; // визуализированная высота dataGrid
                double chh = DgvRES.ColumnHeaderHeight; // высота заголовка

                int countRows = Convert.ToInt32(Math.Floor(Convert.ToDouble(ah) - Convert.ToDouble(chh)) / hs); // количество строк, которое помещается в область видимости = (высота таблицы - заголовок таблицы) / высота строки

                int count = сountRowsAll - countRows; // сколько пустых строк нужно удалить
                int index = -1;
                for (int i = 0; i < count; i++)
                {
                    // Удалить пустые строки в dgv
                    index = ((GlobalRES)DgvRES.DataContext).CollectionRES.ToList().FindIndex(x => x.Id < 0);
                    if (index != -1)
                    {
                        ((GlobalRES)DgvRES.DataContext).CollectionRES.RemoveAt(index);
                    }
                }

                List<TableRes> list = new List<TableRes>();
                for (int i = 0; i < countRows - сountRowsAll; i++)
                {
                    TableRes strRES = new TableRes
                    {
                        Id = -2,
                        FrequencyMHz = 0,
                        Bearing = -2F,
                        Pulse = new ParamDP { Duration = -2F, Period = -2F },
                        Series = new ParamDP { Duration = -2F, Period = -2F },
                        TargetType = string.Empty,
                        ModulationType = (ModulationType)(byte)255,
                        Note = string.Empty,
                        AntennaMaster = (byte)255,
                        AntennaSlave = (byte)255,
                        AmplitudeMaster = -2D,
                        AmplitudeSlave = -2D
                    };
                    list.Add(strRES);
                }

                for (int i = 0; i < list.Count; i++)
                {
                    ((GlobalRES)DgvRES.DataContext).CollectionRES.Add(list[i]);
                }
            }
            catch { }
        }

        /// <summary>
        /// Удалить пустые строки из таблицы
        /// </summary>
        private void DeleteEmptyRows()
        {
            try
            {
                int countEmptyRows = ((GlobalRES)DgvRES.DataContext).CollectionRES.Count(s => s.Id < 0);
                int countAllRows = ((GlobalRES)DgvRES.DataContext).CollectionRES.Count;
                int iCount = countAllRows - countEmptyRows;
                for (int i = iCount; i < countAllRows; i++)
                {
                    ((GlobalRES)DgvRES.DataContext).CollectionRES.RemoveAt(iCount);
                }
            }
            catch { }
        }

        private bool IsSelectedRowEmpty()
        {
            try
            {
                if (((TableRes)DgvRES.SelectedItem).Id == -2)
                    return false;
            }
            catch { }

            return true;
        }
    }
}
