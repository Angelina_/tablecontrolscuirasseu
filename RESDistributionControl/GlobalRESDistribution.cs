﻿using CuirassUModelsDBLib;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace RESDistributionControl
{
    public class GlobalRESDistribution
    {
        public ObservableCollection<TableResDistribution> CollectionRESDistribution { get; set; }

        public GlobalRESDistribution()
        {
            try
            {
                CollectionRESDistribution = new ObservableCollection<TableResDistribution> { };

                //CollectionRESDistribution = new ObservableCollection<TableResDistribution>
                //{
                //    new TableResDistribution
                //    {
                //        Id = 1,
                //        FrequencyKHz = 569599.98,
                //        Bearing = 245.7878F,
                //        Pulse = new ParamDP { Duration = 10550.6F, Period = 0.233430F },
                //        Series = new ParamDP { Duration = 340.887F, Period = 45.34343F },
                //        TargetType = "QQQ",
                //        AntennaMaster = (byte)1,
                //        AntennaSlave = (byte)3,
                //        ReceiverL = ReceiveSignal.YesSignal,
                //        Receiver1 = ReceiveSignal.NoAnswer,
                //        Receiver2 = ReceiveSignal.NoSignal,
                //        Receiver3 = ReceiveSignal.BadSignal,
                //        Track = new ObservableCollection<TableTrackDistribution>
                //        { 
                //            new TableTrackDistribution 
                //            { 
                //                Id = 1,
                //                Coordinates = new Coord { Latitude = 53.76566655, Longitude = 27.4545353, Altitude = 250 } 
                //            },
                //            new TableTrackDistribution
                //            {
                //                Id = 2,
                //                Coordinates = new Coord { Latitude = 51.522555, Longitude = -32.11115353, Altitude = 478 }
                //            },
                //            new TableTrackDistribution
                //            {
                //                Id = 3,
                //                Coordinates = new Coord { Latitude = 57.522555, Longitude = -31.11115353, Altitude = 478 }
                //            }
                //        },
                //        Note = "Jk8 dflk okl"
                //    },
                //    new TableResDistribution
                //    {
                //        Id = 2,
                //        FrequencyKHz = 345666.4668,
                //        Bearing = 55.65578F,
                //        Pulse = new ParamDP { Duration = 200.1F, Period = 420.77F },
                //        Series = new ParamDP { Duration = 112.33F, Period = 56.5F },
                //        TargetType = "54 bjg",
                //        AntennaMaster = (byte)2,
                //        AntennaSlave = (byte)4,
                //        ReceiverL = ReceiveSignal.BadSignal,
                //        Receiver1 = ReceiveSignal.YesSignal,
                //        Receiver2 = ReceiveSignal.NoAnswer,
                //        Receiver3 = ReceiveSignal.NoSignal,
                //        Track = new ObservableCollection<TableTrackDistribution>
                //        {
                //            new TableTrackDistribution
                //            {
                //                Id = 1,
                //                Coordinates = new Coord { Latitude = 54.522555, Longitude = -30.11115353, Altitude = 478 }
                //            },
                //            new TableTrackDistribution
                //            {
                //                Id = 2,
                //                Coordinates = new Coord { Latitude = 57.522555, Longitude = -24.11115353, Altitude = 478 }
                //            }
                //        },
                //        Note = "jkdsgj 878"
                //    }
                //};
            }
            catch (Exception ex) { MessageBox.Show(ex.Message); }
        }
    }
}
