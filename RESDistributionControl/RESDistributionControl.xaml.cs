﻿using CuirassUModelsDBLib;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using TableEvents;

namespace RESDistributionControl
{
    /// <summary>
    /// Логика взаимодействия для UserControl1.xaml
    /// </summary>
    public partial class UserControlRESDistribution : UserControl
    {
        #region Events
        public event EventHandler<TableEvent> OnDeleteRecord = (object sender, TableEvent data) => { };
        public event EventHandler<NameTable> OnClearRecords = (object sender, NameTable data) => { };
        public event EventHandler<TableEvent> OnArchiveRecord = (object sender, TableEvent data) => { };
        #endregion

        public UserControlRESDistribution()
        {
            InitializeComponent();

            DgvRESDistribution.DataContext = new GlobalRESDistribution();

            // TEST --------------------------------------------------------------
            GlobalRESDistribution global = new GlobalRESDistribution();
            ListRESDistribution = global.CollectionRESDistribution.ToList();
            // -------------------------------------------------------------- TEST 
        }

        private void ButtonDelete_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if ((TableResDistribution)DgvRESDistribution.SelectedItem != null)
                {
                    if (!IsSelectedRowEmpty())
                        return;

                    TableResDistribution tableRESDistribution = new TableResDistribution
                    {
                        Id = ((TableResDistribution)DgvRESDistribution.SelectedItem).Id
                    };

                    // Событие удаления одной записи
                    OnDeleteRecord(this, new TableEvent(tableRESDistribution));
                }
            }
            catch { }
        }

        private void ButtonClear_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                // Событие удаления записей
                OnClearRecords(this, NameTable.TableResDistribution);
            }
            catch { }
        }

        private void DgvRESDistribution_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            AddEmptyRows();
        }

        private void DgvRESDistribution_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if ((TableResDistribution)DgvRESDistribution.SelectedItem == null) return;

            if (((TableResDistribution)DgvRESDistribution.SelectedItem).Id > -1)
            {
                if (((TableResDistribution)DgvRESDistribution.SelectedItem).Id != PropNumRESDistribution.SelectedNumRESDistribution)
                {
                    PropNumRESDistribution.SelectedNumRESDistribution = ((TableResDistribution)DgvRESDistribution.SelectedItem).Id;
                    PropNumRESDistribution.IsSelectedNumRESDistribution = true;

                    //OnSelectedRow(this, new SelectedRowEvents(PropNumRESDistribution.SelectedNumRESDistribution));
                }
            }
            else
            {
                PropNumRESDistribution.SelectedNumRESDistribution = 0;
                PropNumRESDistribution.IsSelectedNumRESDistribution = false;
            }
        }

        private void ButtonArchive_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if ((TableResDistribution)DgvRESDistribution.SelectedItem != null && ListRESDistribution != null && ListRESDistribution.Count() > 0)
                {
                    if (!IsSelectedRowEmpty())
                        return;

                    TableResDistribution tableDistribution = ListRESDistribution.Find(x => x.Id == ((TableResDistribution)DgvRESDistribution.SelectedItem).Id);

                    ObservableCollection<TableTrackArchive> listTrackArchive = new ObservableCollection<TableTrackArchive>();

                    for (int i = 0; i < tableDistribution.Track.Count; i++)
                    {
                        TableTrackArchive table = new TableTrackArchive()
                        {
                            Id = tableDistribution.Track[i].Id,
                            TableResId = tableDistribution.Track[i].TableResId,
                            Tau1 = tableDistribution.Track[i].Tau1,
                            Tau2 = tableDistribution.Track[i].Tau2,
                            Tau3 = tableDistribution.Track[i].Tau3,
                            Tau4 = tableDistribution.Track[i].Tau4,
                            Coordinates = tableDistribution.Track[i].Coordinates,
                            Time = tableDistribution.Track[i].Time
                        };
                        listTrackArchive.Add(table);
                    }

                    DateTime dateTimeStart = listTrackArchive.ToList().Min(minTime => minTime.Time);
                    DateTime dateTimeStop = listTrackArchive.ToList().Max(maxTime => maxTime.Time);

                    TableResArchive tableRESArchive = new TableResArchive
                    {
                        Id = ((TableResDistribution)DgvRESDistribution.SelectedItem).Id,
                        FrequencyMHz = ((TableResDistribution)DgvRESDistribution.SelectedItem).FrequencyMHz,
                        Pulse = ((TableResDistribution)DgvRESDistribution.SelectedItem).Pulse,
                        Series = ((TableResDistribution)DgvRESDistribution.SelectedItem).Series,
                        TargetType = ((TableResDistribution)DgvRESDistribution.SelectedItem).TargetType,
                        AntennaMaster = ((TableResDistribution)DgvRESDistribution.SelectedItem).AntennaMaster,
                        AntennaSlave = ((TableResDistribution)DgvRESDistribution.SelectedItem).AntennaSlave,
                        Track = listTrackArchive,
                        DateTimeStart = dateTimeStart,
                        DateTimeStop = dateTimeStop,
                        Note = ((TableResDistribution)DgvRESDistribution.SelectedItem).Note

                    };

                    // Отправить в таблицу ИРИ архив
                    OnArchiveRecord(this, new TableEvent(tableRESArchive));
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message.ToString());
            }
        }
    }
}
