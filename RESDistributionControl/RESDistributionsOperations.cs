﻿using CuirassUModelsDBLib;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using TableEvents;

namespace RESDistributionControl
{
    public partial class UserControlRESDistribution : UserControl
    {
        /// <summary>
        /// Обновить контрол
        /// </summary>
        /// <param name="listRESDistribution"></param>
        private List<TableResDistribution> listRESDistribution = new List<TableResDistribution> { };
        public List<TableResDistribution> ListRESDistribution
        {
            get { return listRESDistribution; }
            set
            {
                if (listRESDistribution != null && listRESDistribution.Equals(value)) return;
                listRESDistribution = value;
                UpdateRESDistribution();
            }
        }

        private void UpdateRESDistribution()
        {
            try
            {
                DeleteEmptyRows();

                if (ListRESDistribution == null)
                    return;

                ((GlobalRESDistribution)DgvRESDistribution.DataContext).CollectionRESDistribution.Clear();

                for (int i = 0; i < ListRESDistribution.Count; i++)
                {
                    ((GlobalRESDistribution)DgvRESDistribution.DataContext).CollectionRESDistribution.Add(ListRESDistribution[i]);
                }

                AddEmptyRows();

                DgvRESDistribution.SelectedIndex = PropNumRESDistribution.SelectedNumRESDistribution;

            }
            catch { }
        }

        /// <summary>
        /// Добавить несколько записей в контрол
        /// </summary>
        /// <param name="listRESDistribution"></param>
        public void AddRESDistribution(List<TableResDistribution> listRESDistribution)
        {
            try
            {
                DeleteEmptyRows();

                for (int i = 0; i < listRESDistribution.Count; i++)
                {
                    int ind = ((GlobalRESDistribution)DgvRESDistribution.DataContext).CollectionRESDistribution.ToList().FindIndex(x => x.Id == listRESDistribution[i].Id);
                    if (ind != -1)
                    {
                        ((GlobalRESDistribution)DgvRESDistribution.DataContext).CollectionRESDistribution[ind] = listRESDistribution[i];
                    }
                    else
                    {
                        ((GlobalRESDistribution)DgvRESDistribution.DataContext).CollectionRESDistribution.Add(listRESDistribution[i]);
                    }
                }

                //AddEmptyRows();

                DgvRESDistribution.SelectedIndex = PropNumRESDistribution.SelectedNumRESDistribution;
            }
            catch { }
        }

        /// <summary>
        /// Удалить одну запись из контрола
        /// </summary>
        public void DeleteRESDistribution(TableResDistribution tableRESDistribution)
        {
            try
            {
                int index = ((GlobalRESDistribution)DgvRESDistribution.DataContext).CollectionRESDistribution.ToList().FindIndex(x => x.Id == tableRESDistribution.Id);
                ((GlobalRESDistribution)DgvRESDistribution.DataContext).CollectionRESDistribution.RemoveAt(index);

                AddEmptyRows();
            }
            catch { }
        }

        /// <summary>
        /// Удалить все записи из контрола
        /// </summary>
        public void ClearRESDistribution()
        {
            try
            {
                ((GlobalRESDistribution)DgvRESDistribution.DataContext).CollectionRESDistribution.Clear();

                AddEmptyRows();
            }
            catch { }
        }

        /// <summary>
        /// Добавить в таблицу пустые строки
        /// </summary>
        private void AddEmptyRows()
        {
            try
            {
                int сountRowsAll = DgvRESDistribution.Items.Count; // количество всех строк в таблице
                double hs = 23; // высота строки
                double ah = DgvRESDistribution.ActualHeight; // визуализированная высота dataGrid
                double chh = DgvRESDistribution.ColumnHeaderHeight; // высота заголовка

                int countRows = Convert.ToInt32(Math.Floor(Convert.ToDouble(ah) - Convert.ToDouble(chh)) / hs); // количество строк, которое помещается в область видимости = (высота таблицы - заголовок таблицы) / высота строки

                int count = сountRowsAll - countRows; // сколько пустых строк нужно удалить
                int index = -1;
                for (int i = 0; i < count; i++)
                {
                    // Удалить пустые строки в dgv
                    index = ((GlobalRESDistribution)DgvRESDistribution.DataContext).CollectionRESDistribution.ToList().FindIndex(x => x.Id < 0);
                    if (index != -1)
                    {
                        ((GlobalRESDistribution)DgvRESDistribution.DataContext).CollectionRESDistribution.RemoveAt(index);
                    }
                }

                List<TableResDistribution> list = new List<TableResDistribution>();
                for (int i = 0; i < countRows - сountRowsAll; i++)
                {
                    TableResDistribution strRESDistribution = new TableResDistribution
                    {
                        Id = -2,
                        FrequencyMHz = 0,
                        Bearing = -2F,
                        Pulse = new ParamDP { Duration = -2F, Period = -2F },
                        Series = new ParamDP { Duration = -2F, Period = -2F },
                        TargetType = string.Empty,
                        AntennaMaster = (byte)255,
                        AntennaSlave = (byte)255,
                        ReceiverL = (ReceiveSignal)(byte)255,
                        Receiver1 = (ReceiveSignal)(byte)255,
                        Receiver2 = (ReceiveSignal)(byte)255,
                        Receiver3 = (ReceiveSignal)(byte)255,
                        Track = new ObservableCollection<TableTrackDistribution>
                        {
                            new TableTrackDistribution
                            {
                                Coordinates = new Coord { Latitude = -2, Longitude = -2, Altitude = -2 }
                            }
                        },
                        Note = string.Empty
                    };
                    list.Add(strRESDistribution);
                }

                for (int i = 0; i < list.Count; i++)
                {
                    ((GlobalRESDistribution)DgvRESDistribution.DataContext).CollectionRESDistribution.Add(list[i]);
                }
            }
            catch { }
        }

        /// <summary>
        /// Удалить пустые строки из таблицы
        /// </summary>
        private void DeleteEmptyRows()
        {
            try
            {
                int countEmptyRows = ((GlobalRESDistribution)DgvRESDistribution.DataContext).CollectionRESDistribution.Count(s => s.Id < 0);
                int countAllRows = ((GlobalRESDistribution)DgvRESDistribution.DataContext).CollectionRESDistribution.Count;
                int iCount = countAllRows - countEmptyRows;
                for (int i = iCount; i < countAllRows; i++)
                {
                    ((GlobalRESDistribution)DgvRESDistribution.DataContext).CollectionRESDistribution.RemoveAt(iCount);
                }
            }
            catch { }
        }

        private bool IsSelectedRowEmpty()
        {
            try
            {
                if (((TableResDistribution)DgvRESDistribution.SelectedItem).Id == -2)
                    return false;
            }
            catch { }

            return true;
        }
    }
}
