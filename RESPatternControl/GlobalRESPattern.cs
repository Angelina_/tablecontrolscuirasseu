﻿using CuirassUModelsDBLib;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace RESPatternControl
{
    public class GlobalRESPattern
    {
        public ObservableCollection<TableResPattern> CollectionRESPattern { get; set; }

        public GlobalRESPattern()
        {
            try
            {
                CollectionRESPattern = new ObservableCollection<TableResPattern> { };

                //CollectionRESPattern = new ObservableCollection<TableResPattern>
                //{
                //    new TableResPattern
                //    {
                //        Id = 1,
                //        FrequencyMinKHz = 569000.98,
                //        FrequencyMaxKHz = 945000.7878,
                //        Pulse = new ParamDP{ Duration = 10550.6F, Period = 0.233430F },
                //        Series = new ParamDP{ Duration = 340.887F, Period = 45.34343F },
                //        Name = "Jk8 dflk okl",
                //        ModulationType = ModulationType.LFM,
                //        WorkingMode = WorkingMode.AirLand,
                //        TargetClass = ClassTarget.Radar
                //    },
                //    new TableResPattern
                //    {
                //        Id = 2,
                //        FrequencyMinKHz = 769678.98,
                //        FrequencyMaxKHz = 4566567.7878,
                //        Pulse = new ParamDP{ Duration = 500.3F, Period = 320.733F },
                //        Series = new ParamDP{ Duration = 242.43F, Period = 226.2F },
                //        Name = "dsf dw",
                //        ModulationType = ModulationType.FM,
                //        WorkingMode = WorkingMode.AirAir,
                //        TargetClass = ClassTarget.Helicopter
                //    }
                //};
            }
            catch (Exception ex) { MessageBox.Show(ex.Message); }
        }
    }
}
