﻿using CuirassUModelsDBLib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using TableEvents;

namespace RESPatternControl
{
    /// <summary>
    /// Логика взаимодействия для UserControl1.xaml
    /// </summary>
    public partial class UserControlRESPattern : UserControl
    {
        public RESPatternProperty RESPatternWindow;

        #region Events
        public event EventHandler<TableEvent> OnAddRecord = (object sender, TableEvent data) => { };
        public event EventHandler<TableEvent> OnChangeRecord = (object sender, TableEvent data) => { };
        public event EventHandler<TableEvent> OnDeleteRecord = (object sender, TableEvent data) => { };
        public event EventHandler<NameTable> OnClearRecords = (object sender, NameTable data) => { };

        // Открылось окно с PropertyGrid
        public event EventHandler<RESPatternProperty> OnIsWindowPropertyOpen = (object sender, RESPatternProperty data) => { };
        #endregion

        public UserControlRESPattern()
        {
            InitializeComponent();

            DgvRESPattern.DataContext = new GlobalRESPattern();
        }

        private void ButtonAdd_Click(object sender, RoutedEventArgs e)
        {
            RESPatternWindow = new RESPatternProperty(((GlobalRESPattern)DgvRESPattern.DataContext).CollectionRESPattern);

            OnIsWindowPropertyOpen(this, RESPatternWindow);

            if (RESPatternWindow.ShowDialog() == true)
            {
                RESPatternWindow.RESPattern.FrequencyMinMHz = RESPatternWindow.RESPattern.FrequencyMinMHz;
                RESPatternWindow.RESPattern.FrequencyMaxMHz = RESPatternWindow.RESPattern.FrequencyMaxMHz;

                // Событие добавления одной записи
                OnAddRecord(this, new TableEvent(RESPatternWindow.RESPattern));
            }
        }

        private void ButtonChange_Click(object sender, RoutedEventArgs e)
        {
            if ((TableResPattern)DgvRESPattern.SelectedItem != null)
            {
                if (((TableResPattern)DgvRESPattern.SelectedItem).Id > 0)
                {
                    var selected = (TableResPattern)DgvRESPattern.SelectedItem;

                    RESPatternWindow = new RESPatternProperty(((GlobalRESPattern)DgvRESPattern.DataContext).CollectionRESPattern, selected.Clone());

                    OnIsWindowPropertyOpen(this, RESPatternWindow);

                    if (RESPatternWindow.ShowDialog() == true)
                    {
                        RESPatternWindow.RESPattern.FrequencyMinMHz = RESPatternWindow.RESPattern.FrequencyMinMHz;
                        RESPatternWindow.RESPattern.FrequencyMaxMHz = RESPatternWindow.RESPattern.FrequencyMaxMHz;

                        // Событие изменения одной записи
                        OnChangeRecord(this, new TableEvent(RESPatternWindow.RESPattern));
                    }
                }
            }
        }

        private void ButtonDelete_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if ((TableResPattern)DgvRESPattern.SelectedItem != null)
                {
                    if (!IsSelectedRowEmpty())
                        return;

                    TableResPattern tableRESPattern = new TableResPattern
                    {
                        Id = ((TableResPattern)DgvRESPattern.SelectedItem).Id
                    };

                    // Событие удаления одной записи
                    OnDeleteRecord(this, new TableEvent(tableRESPattern));
                }
            }
            catch { }
        }

        private void ButtonClear_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                // Событие удаления записей
                OnClearRecords(this, NameTable.TableResPattern);
            }
            catch { }
        }

        private void ButtonSaveFile_Click(object sender, RoutedEventArgs e)
        {

        }

        private void ButtonLoadFile_Click(object sender, RoutedEventArgs e)
        {

        } 
        
        private void DgvRESPattern_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            AddEmptyRows();
        }

        private void DgvRESPattern_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }
    }
}
