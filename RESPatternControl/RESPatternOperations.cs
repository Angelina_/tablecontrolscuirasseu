﻿using CuirassUModelsDBLib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using TableEvents;

namespace RESPatternControl
{
    public partial class UserControlRESPattern : UserControl
    {
        /// <summary>
        /// Обновить контрол
        /// </summary>
        /// <param name="listRESPattern"></param>
        public void UpdateRESPattern(List<TableResPattern> listRESPattern)
        {
            try
            {
                DeleteEmptyRows();
                if (listRESPattern == null)
                    return;

                ((GlobalRESPattern)DgvRESPattern.DataContext).CollectionRESPattern.Clear();

                for (int i = 0; i < listRESPattern.Count; i++)
                {
                    ((GlobalRESPattern)DgvRESPattern.DataContext).CollectionRESPattern.Add(listRESPattern[i]);
                }

                AddEmptyRows();

            }
            catch { }
        }

        /// <summary>
        /// Добавить несколько записей в контрол
        /// </summary>
        /// <param name="listRESPattern"></param>
        public void AddRESPattern(List<TableResPattern> listRESPattern)
        {
            try
            {
                DeleteEmptyRows();

                for (int i = 0; i < listRESPattern.Count; i++)
                {
                    int ind = ((GlobalRESPattern)DgvRESPattern.DataContext).CollectionRESPattern.ToList().FindIndex(x => x.Id == listRESPattern[i].Id);
                    if (ind != -1)
                    {
                        ((GlobalRESPattern)DgvRESPattern.DataContext).CollectionRESPattern[ind] = listRESPattern[i];
                    }
                    else
                    {
                        ((GlobalRESPattern)DgvRESPattern.DataContext).CollectionRESPattern.Add(listRESPattern[i]);
                    }
                }

                AddEmptyRows();

                DgvRESPattern.SelectedIndex = PropNumRESPattern.SelectedNumRESPattern;
            }
            catch { }
        }

        /// <summary>
        /// Удалить одну запись из контрола
        /// </summary>
        public void DeleteRESPattern(TableResPattern tableRESPattern)
        {
            try
            {
                int index = ((GlobalRESPattern)DgvRESPattern.DataContext).CollectionRESPattern.ToList().FindIndex(x => x.Id == tableRESPattern.Id);
                ((GlobalRESPattern)DgvRESPattern.DataContext).CollectionRESPattern.RemoveAt(index);

                AddEmptyRows();
            }
            catch { }
        }

        /// <summary>
        /// Удалить все записи из контрола
        /// </summary>
        public void ClearRESPattern()
        {
            try
            {
                ((GlobalRESPattern)DgvRESPattern.DataContext).CollectionRESPattern.Clear();

                AddEmptyRows();
            }
            catch { }
        }

        /// <summary>
        /// Добавить в таблицу пустые строки
        /// </summary>
        private void AddEmptyRows()
        {
            try
            {
                int сountRowsAll = DgvRESPattern.Items.Count; // количество всех строк в таблице
                double hs = 23; // высота строки
                double ah = DgvRESPattern.ActualHeight; // визуализированная высота dataGrid
                double chh = DgvRESPattern.ColumnHeaderHeight; // высота заголовка

                int countRows = Convert.ToInt32(Math.Floor(Convert.ToDouble(ah) - Convert.ToDouble(chh)) / hs); // количество строк, которое помещается в область видимости = (высота таблицы - заголовок таблицы) / высота строки

                int count = сountRowsAll - countRows; // сколько пустых строк нужно удалить
                int index = -1;
                for (int i = 0; i < count; i++)
                {
                    // Удалить пустые строки в dgv
                    index = ((GlobalRESPattern)DgvRESPattern.DataContext).CollectionRESPattern.ToList().FindIndex(x => x.Id < 0);
                    if (index != -1)
                    {
                        ((GlobalRESPattern)DgvRESPattern.DataContext).CollectionRESPattern.RemoveAt(index);
                    }
                }

                List<TableResPattern> list = new List<TableResPattern>();
                for (int i = 0; i < countRows - сountRowsAll; i++)
                {
                    TableResPattern strRESPattern = new TableResPattern
                    {
                        Id = -2,
                        FrequencyMinMHz = 0,
                        FrequencyMaxMHz = 0,
                        Pulse = new ParamDP { Duration = -2F, Period = -2F },
                        Series = new ParamDP { Duration = -2F, Period = -2F },
                        Name = string.Empty,
                        ModulationType = (ModulationType)(byte)255,
                        WorkingMode = (WorkingMode)(byte)255,
                        TargetClass = (ClassTarget)(byte)255
                    };
                    list.Add(strRESPattern);
                }

                for (int i = 0; i < list.Count; i++)
                {
                    ((GlobalRESPattern)DgvRESPattern.DataContext).CollectionRESPattern.Add(list[i]);
                }
            }
            catch { }
        }

        /// <summary>
        /// Удалить пустые строки из таблицы
        /// </summary>
        private void DeleteEmptyRows()
        {
            try
            {
                int countEmptyRows = ((GlobalRESPattern)DgvRESPattern.DataContext).CollectionRESPattern.Count(s => s.Id < 0);
                int countAllRows = ((GlobalRESPattern)DgvRESPattern.DataContext).CollectionRESPattern.Count;
                int iCount = countAllRows - countEmptyRows;
                for (int i = iCount; i < countAllRows; i++)
                {
                    ((GlobalRESPattern)DgvRESPattern.DataContext).CollectionRESPattern.RemoveAt(iCount);
                }
            }
            catch { }
        }

        private bool IsSelectedRowEmpty()
        {
            try
            {
                if (((TableResPattern)DgvRESPattern.SelectedItem).Id == -2)
                    return false;
            }
            catch { }

            return true;
        }
    }
}
