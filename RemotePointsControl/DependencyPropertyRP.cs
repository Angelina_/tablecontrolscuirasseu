﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using TableEvents;

namespace RemotePointsControl
{
    public partial class UserControlRemotePoints : UserControl, INotifyPropertyChanged
    {
        #region INotifyPropertyChanged
        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged([CallerMemberName]string prop = "")
        {
            try
            {
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));
            }
            catch { }
        }
        #endregion



        #region FormatViewCoord

        public static readonly DependencyProperty ViewCoordProperty = DependencyProperty.Register("ViewCoord", typeof(byte), typeof(UserControlRemotePoints),
                                                                       new PropertyMetadata((byte)1, new PropertyChangedCallback(ViewCoordChanged)));

        public byte ViewCoord
        {
            get { return (byte)GetValue(ViewCoordProperty); }
            set { SetValue(ViewCoordProperty, value); }
        }

        private static void ViewCoordChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            try
            {
                UserControlRemotePoints userControlRemotePoints = (UserControlRemotePoints)d;
                userControlRemotePoints.UpdateFormatCoords();
            }
            catch
            { }

        }
        #endregion

        /// <summary>
        /// Обновить вид координат в таблице
        /// </summary>
        private void UpdateFormatCoords()
        {
            try
            {
                if (TempListRemotePoints == null) return;

                PropViewCoords.ViewCoords = ViewCoord;

                ((GlobalRemotePoints)DgvRemotePoints.DataContext).CollectionRemotePoints.Clear();

                for (int i = 0; i < TempListRemotePoints.Count; i++)
                {
                    ((GlobalRemotePoints)DgvRemotePoints.DataContext).CollectionRemotePoints.Add(TempListRemotePoints[i]);
                }

                AddEmptyRows();

                int ind = ((GlobalRemotePoints)DgvRemotePoints.DataContext).CollectionRemotePoints.ToList().FindIndex(x => x.Id == PropNumRemotePoints.SelectedNumRemotePoints);
                if (ind != -1)
                {
                    DgvRemotePoints.SelectedIndex = ind;
                }
                else
                {
                    DgvRemotePoints.SelectedIndex = 0;
                }
            }
            catch { }
        }

       
    }
}
