﻿using CuirassUModelsDBLib;
using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Runtime.Serialization;
using System.Windows;
using System.Windows.Controls.WpfPropertyGrid;

namespace RemotePointsControl
{
    public class GlobalRemotePoints
    {
        public ObservableCollection<MyTableRemotePoints> CollectionRemotePoints { get; set; }

        public GlobalRemotePoints()
        {
            try
            {
                CollectionRemotePoints = new ObservableCollection<MyTableRemotePoints> { };

                /*CollectionRemotePoints = new ObservableCollection<MyTableRemotePoints>
                {
                    new MyTableRemotePoints
                    {
                        Id = 1,
                        Coordinates = new Coord
                        {
                            Latitude = 53.7899777,
                            Longitude = 27.77565,
                            Altitude = 150
                        },
                        CourseAngle = 45.0F,
                        TimeError = 100,
                        IpAddress = "127.0.0.1",
                        Port = 10002,
                        Note = "Jk8 dflk okl"
                    },
                    new MyTableRemotePoints
                    {
                        Id = 2,
                        Coordinates = new Coord
                        {
                            Latitude = 54.643233,
                            Longitude = 30.746425,
                            Altitude = 245
                        },
                        CourseAngle = 124.6F,
                        TimeError = 33,
                        IpAddress = "192.168.0.12",
                        Port = 8090,
                        Note = "jkdsgj 878"
                    }
                };*/
            }
            catch (Exception ex) { MessageBox.Show(ex.Message); }
        }
    }

    [DataContract]
    [CategoryOrder("ID", 1)]
    [CategoryOrder("Общие", 2)]
    [CategoryOrder(nameof(Coordinates), 3)]
    [CategoryOrder("Прочее", 4)]
    [KnownType(typeof(AbstractCommonTable))]
    public class MyTableRemotePoints
    {
        [DataMember]
        [Category("ID")]
        [DisplayName(nameof(Id)), ReadOnly(false)]
        [Browsable(false)]
        [PropertyOrder(1)]
        public int Id { get; set; }

        [DataMember]
        [Category(nameof(Coordinates))]
        [DisplayName(" ")]
        [TypeConverter(typeof(ExpandableObjectConverter))]
        public Coord Coordinates { get; set; } = new Coord();

        [DataMember]
        [Category("Общие")]
        [DisplayName(nameof(CourseAngle))]
        [PropertyOrder(1)]
        public float CourseAngle { get; set; }

        [DataMember]
        [Category("Общие")]
        [DisplayName(nameof(TimeError))]
        [PropertyOrder(2)]
        public short TimeError { get; set; }

        [DataMember]
        [Category("Общие")]
        [DisplayName(nameof(IpAddress))]
        [PropertyOrder(3)]
        public string IpAddress { get; set; } = String.Empty;

        [DataMember]
        [Category("Общие")]
        [DisplayName(nameof(Port))]
        [PropertyOrder(4)]
        public int Port { get; set; }

        [DataMember]
        [Category("Прочее")]
        [DisplayName(nameof(Note))]
        public string Note { get; set; } = String.Empty;

        [DataMember]
        [Category(nameof(Coordinates))]
        [DisplayName(" ")]
        [TypeConverter(typeof(ExpandableObjectConverter))]
        public CoordDDMMSS CoordinatesDDMMSS { get; set; } = new CoordDDMMSS();

        public MyTableRemotePoints Clone()
        {
            return new MyTableRemotePoints
            {
                Id = Id,
                Coordinates = new Coord()
                {
                    Altitude = Coordinates.Altitude,
                    Longitude = Coordinates.Longitude,
                    Latitude = Coordinates.Latitude,
                },
                CourseAngle = CourseAngle,
                TimeError = TimeError,
                IpAddress = IpAddress,
                Port = Port,
                Note = Note,
                CoordinatesDDMMSS = new CoordDDMMSS
                {
                    LatDegrees = CoordinatesDDMMSS.LatDegrees,
                    LonDegrees = CoordinatesDDMMSS.LonDegrees,
                    LatMinutesDDMM = CoordinatesDDMMSS.LatMinutesDDMM,
                    LonMinutesDDMM = CoordinatesDDMMSS.LonMinutesDDMM,
                    LatMinutesDDMMSS = CoordinatesDDMMSS.LatMinutesDDMMSS,
                    LonMinutesDDMMSS = CoordinatesDDMMSS.LonMinutesDDMMSS,
                    LatSeconds = CoordinatesDDMMSS.LatSeconds,
                    LonSeconds = CoordinatesDDMMSS.LonSeconds,
                    Altitude = CoordinatesDDMMSS.Altitude
                }
            };
        }
    }
    public class CoordDDMMSS : INotifyPropertyChanged
    {
        #region NotifyPropertyChanged

        public event PropertyChangedEventHandler PropertyChanged;

        public void OnPropertyChanged([CallerMemberName] string prop = "")
        {
            try
            {
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));
            }
            catch { }
        }
        #endregion

        private int latDegrees = 0;
        [DataMember]
        [DisplayName(nameof(LatDegrees))]
        [NotifyParentProperty(true)]
        [Category(nameof(Coord))]
        public int LatDegrees
        {
            get { return latDegrees; }
            set
            {
                if (latDegrees == value)
                    return;

                latDegrees = value;
                OnPropertyChanged(nameof(LatDegrees));
            }
        }

        private int lonDegrees = 0;
        [DataMember]
        [DisplayName(nameof(LonDegrees))]
        [NotifyParentProperty(true)]
        [Category(nameof(Coord))]
        public int LonDegrees
        {
            get { return lonDegrees; }
            set
            {
                if (lonDegrees == value)
                    return;

                lonDegrees = value;
                OnPropertyChanged(nameof(LonDegrees));
            }
        }

        private double latMinutesDDMM = 0;
        [DataMember]
        [DisplayName(nameof(LatMinutesDDMM))]
        [NotifyParentProperty(true)]
        [Category(nameof(Coord))]
        public double LatMinutesDDMM
        {
            get { return latMinutesDDMM; }
            set
            {
                if (latMinutesDDMM == value)
                    return;

                latMinutesDDMM = value;
                OnPropertyChanged(nameof(LatMinutesDDMM));
            }
        }

        private double lonMinutesDDMM = 0;
        [DataMember]
        [DisplayName(nameof(LonMinutesDDMM))]
        [NotifyParentProperty(true)]
        [Category(nameof(Coord))]
        public double LonMinutesDDMM
        {
            get { return lonMinutesDDMM; }
            set
            {
                if (lonMinutesDDMM == value)
                    return;

                lonMinutesDDMM = value;
                OnPropertyChanged(nameof(LonMinutesDDMM));
            }
        }

        private int latMinutesDDMMSS = 0;
        [DataMember]
        [DisplayName(nameof(LatMinutesDDMMSS))]
        [NotifyParentProperty(true)]
        [Category(nameof(Coord))]
        public int LatMinutesDDMMSS
        {
            get { return latMinutesDDMMSS; }
            set
            {
                if (latMinutesDDMMSS == value)
                    return;

                latMinutesDDMMSS = value;
                OnPropertyChanged(nameof(LatMinutesDDMMSS));
            }
        }

        private int lonMinutesDDMMSS = 0;
        [DataMember]
        [DisplayName(nameof(LonMinutesDDMMSS))]
        [NotifyParentProperty(true)]
        [Category(nameof(Coord))]
        public int LonMinutesDDMMSS
        {
            get { return lonMinutesDDMMSS; }
            set
            {
                if (lonMinutesDDMMSS == value)
                    return;

                lonMinutesDDMMSS = value;
                OnPropertyChanged(nameof(LonMinutesDDMMSS));
            }
        }

        private double latSeconds = 0;
        [DataMember]
        [DisplayName(nameof(LatSeconds))]
        [NotifyParentProperty(true)]
        [Category(nameof(Coord))]
        public double LatSeconds
        {
            get { return latSeconds; }
            set
            {
                if (latSeconds == value)
                    return;

                latSeconds = value;
                OnPropertyChanged(nameof(LatSeconds));
            }
        }

        private double lonSeconds = 0;
        [DataMember]
        [DisplayName(nameof(LonSeconds))]
        [NotifyParentProperty(true)]
        [Category(nameof(Coord))]
        public double LonSeconds
        {
            get { return lonSeconds; }
            set
            {
                if (lonSeconds == value)
                    return;

                lonSeconds = value;
                OnPropertyChanged(nameof(LonSeconds));
            }
        }

        private float altitude = -1;
        [DataMember]
        [DisplayName(nameof(Altitude))]
        [NotifyParentProperty(true)]
        [Category(nameof(Coord))]
        public float Altitude
        {
            get { return altitude; }
            set
            {
                if (altitude == value)
                    return;

                altitude = value;
                OnPropertyChanged(nameof(Altitude));
            }
        }
    }
}
