﻿using CuirassUModelsDBLib;
using System;
using System.Windows;
using System.Windows.Controls;
using TableEvents;

namespace RemotePointsControl
{
    /// <summary>
    /// Логика взаимодействия для UserControl1.xaml
    /// </summary>
    public partial class UserControlRemotePoints : UserControl
    {
        public RemotePointsProperty RemotePointsWindow;

        #region Events
        public event EventHandler<TableEvent> OnAddRecord = (object sender, TableEvent data) => { };
        public event EventHandler<TableEvent> OnChangeRecord = (object sender, TableEvent data) => { };
        public event EventHandler<TableEvent> OnDeleteRecord = (object sender, TableEvent data) => { };
        public event EventHandler<NameTable> OnClearRecords = (object sender, NameTable data) => { };

        // Открылось окно с PropertyGrid
        public event EventHandler<RemotePointsProperty> OnIsWindowPropertyOpen = (object sender, RemotePointsProperty data) => { };

        public event EventHandler<TableEventReport> OnAddTableToReport = (object sender, TableEventReport data) => { };
        #endregion
        public UserControlRemotePoints()
        {
            InitializeComponent();

            // TEST --------------------------------------------------------------
            PropViewCoords.ViewCoords = ViewCoord;
            ViewCoord = (byte)3;
            PropViewCoords.ViewCoords = 1;
            // -------------------------------------------------------------- TEST 

            DgvRemotePoints.DataContext = new GlobalRemotePoints();
        }

        private void ButtonAdd_Click(object sender, RoutedEventArgs e)
        {
            RemotePointsWindow = new RemotePointsProperty(((GlobalRemotePoints)DgvRemotePoints.DataContext).CollectionRemotePoints);

            OnIsWindowPropertyOpen(this, RemotePointsWindow);

            if (!PropIsRecRemotePoints.IsRecAdd && !PropIsRecRemotePoints.IsRecChange)
            {
                RemotePointsWindow.Show();
                PropIsRecRemotePoints.IsRecAdd = true;
                RemotePointsWindow.OnAddRecordPG += new EventHandler<MyTableRemotePoints>(RemotePointsWindow_OnAddRecordPG);
            }
        }

        private void RemotePointsWindow_OnAddRecordPG(object sender, MyTableRemotePoints e)
        {
            PropIsRecRemotePoints.IsRecAdd = false;

            // Событие добавления одной записи
            OnAddRecord(this, new TableEvent(MyTableRemotePointsToTableRemotePoints(e)));

        }

        private void ButtonChange_Click(object sender, RoutedEventArgs e)
        {
            if ((MyTableRemotePoints)DgvRemotePoints.SelectedItem != null)
            {
                if (((MyTableRemotePoints)DgvRemotePoints.SelectedItem).Id > 0)
                {
                    var selected = (MyTableRemotePoints)DgvRemotePoints.SelectedItem;

                    RemotePointsWindow = new RemotePointsProperty(((GlobalRemotePoints)DgvRemotePoints.DataContext).CollectionRemotePoints, selected.Clone());

                    OnIsWindowPropertyOpen(this, RemotePointsWindow);

                    if (!PropIsRecRemotePoints.IsRecAdd && !PropIsRecRemotePoints.IsRecChange)
                    {
                        RemotePointsWindow.Show();
                        PropIsRecRemotePoints.IsRecChange = true;
                        RemotePointsWindow.OnChangeRecordPG += new EventHandler<MyTableRemotePoints>(RemotePointsWindow_OnChangeRecordPG);
                    }
                }
            }
        }

        private void RemotePointsWindow_OnChangeRecordPG(object sender, MyTableRemotePoints e)
        {
            PropIsRecRemotePoints.IsRecChange = false;

            // Событие изменения одной записи
            OnChangeRecord(this, new TableEvent(MyTableRemotePointsToTableRemotePoints(e)));
        }

        private void ButtonDelete_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if ((MyTableRemotePoints)DgvRemotePoints.SelectedItem != null)
                {
                    if (!IsSelectedRowEmpty())
                        return;

                    TableRemotePoints tableRemotePoints = new TableRemotePoints
                    {
                        Id = ((MyTableRemotePoints)DgvRemotePoints.SelectedItem).Id
                    };

                    // Событие удаления одной записи
                    OnDeleteRecord(this, new TableEvent(tableRemotePoints));
                }
            }
            catch { }
        }

        private void ButtonClear_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                // Событие удаления записей
                OnClearRecords(this, NameTable.TableRemotePoints);
            }
            catch { }
        }

        private void DgvRemotePoints_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            AddEmptyRows();
        }

        private void DgvRemotePoints_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if ((MyTableRemotePoints)DgvRemotePoints.SelectedItem == null) return;

            if (((MyTableRemotePoints)DgvRemotePoints.SelectedItem).Id > -1)
            {
                if (((MyTableRemotePoints)DgvRemotePoints.SelectedItem).Id != PropNumRemotePoints.SelectedNumRemotePoints)
                {
                    PropNumRemotePoints.SelectedNumRemotePoints = ((MyTableRemotePoints)DgvRemotePoints.SelectedItem).Id;
                    PropNumRemotePoints.IsSelectedNumRemotePoints = true;

                    //OnSelectedRow(this, new SelectedRowEvents(PropNumRemotePoints.SelectedNumRemotePoints));
                }
            }
            else
            {
                PropNumRemotePoints.SelectedNumRemotePoints = 0;
                PropNumRemotePoints.IsSelectedNumRemotePoints = false;
            }
        }
    }
}
