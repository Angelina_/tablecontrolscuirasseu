﻿using CuirassUModelsDBLib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using TableEvents;

namespace RemotePointsControl
{
    public partial class UserControlRemotePoints : UserControl
    {
        public List<MyTableRemotePoints> TempListRemotePoints { get; set; }

        /// <summary>
        /// Обновить контрол
        /// </summary>
        /// <param name="listRemotePoints"></param>
        public void UpdateRemotePoints(List<TableRemotePoints> listRemotePoints)
        {
            try
            {
                if (listRemotePoints == null)
                    return;

                ((GlobalRemotePoints)DgvRemotePoints.DataContext).CollectionRemotePoints.Clear();

                List<MyTableRemotePoints> list = TableRemotePointsToMyTableRemotePoints(listRemotePoints);

                TempListRemotePoints = list;

                for (int i = 0; i < listRemotePoints.Count; i++)
                {
                    ((GlobalRemotePoints)DgvRemotePoints.DataContext).CollectionRemotePoints.Add(list[i]);
                }

                AddEmptyRows();

                int ind = ((GlobalRemotePoints)DgvRemotePoints.DataContext).CollectionRemotePoints.ToList().FindIndex(x => x.Id == PropNumRemotePoints.SelectedNumRemotePoints);
                if (ind != -1)
                {
                    DgvRemotePoints.SelectedIndex = ind;
                }
                else
                {
                    DgvRemotePoints.SelectedIndex = 0;
                }


            }
            catch { }
        }

        /// <summary>
        /// Преобразование TableRemotePoints к MyTableRemotePoints
        /// </summary>
        /// <param name="listRemotePoints"></param>
        /// <returns> List<MyTableRemotePoints> </returns>
        private List<MyTableRemotePoints> TableRemotePointsToMyTableRemotePoints(List<TableRemotePoints> listRemotePoints)
        {
            List<MyTableRemotePoints> list = new List<MyTableRemotePoints>();
            for (int i = 0; i < listRemotePoints.Count; i++)
            {
                MyTableRemotePoints table = new MyTableRemotePoints();

                double minutesLat = (listRemotePoints[i].Coordinates.Latitude - Math.Truncate(listRemotePoints[i].Coordinates.Latitude)) * 60;
                double minutesLon = (listRemotePoints[i].Coordinates.Longitude - Math.Truncate(listRemotePoints[i].Coordinates.Longitude)) * 60;

                table.Id = listRemotePoints[i].Id;
                table.Coordinates.Altitude = listRemotePoints[i].Coordinates.Altitude;
                table.Coordinates.Latitude = listRemotePoints[i].Coordinates.Latitude;
                table.Coordinates.Longitude = listRemotePoints[i].Coordinates.Longitude;
                table.CoordinatesDDMMSS.LatDegrees = (int)Math.Truncate(listRemotePoints[i].Coordinates.Latitude);
                table.CoordinatesDDMMSS.LonDegrees = (int)Math.Truncate(listRemotePoints[i].Coordinates.Longitude);
                table.CoordinatesDDMMSS.LatMinutesDDMM = Math.Round((listRemotePoints[i].Coordinates.Latitude - Math.Truncate(listRemotePoints[i].Coordinates.Latitude)) * 60, 4);
                table.CoordinatesDDMMSS.LonMinutesDDMM = Math.Round((listRemotePoints[i].Coordinates.Longitude - Math.Truncate(listRemotePoints[i].Coordinates.Longitude)) * 60, 4);
                table.CoordinatesDDMMSS.LatMinutesDDMMSS = (int)((listRemotePoints[i].Coordinates.Latitude - Math.Truncate(listRemotePoints[i].Coordinates.Latitude)) * 60);
                table.CoordinatesDDMMSS.LonMinutesDDMMSS = (int)((listRemotePoints[i].Coordinates.Longitude - Math.Truncate(listRemotePoints[i].Coordinates.Longitude)) * 60);
                table.CoordinatesDDMMSS.LatSeconds = Math.Round((minutesLat - Math.Truncate(minutesLat)) * 60, 2);
                table.CoordinatesDDMMSS.LonSeconds = Math.Round((minutesLon - Math.Truncate(minutesLon)) * 60, 2);
                table.CoordinatesDDMMSS.Altitude = listRemotePoints[i].Coordinates.Altitude;
                table.CourseAngle = listRemotePoints[i].CourseAngle;
                table.TimeError = listRemotePoints[i].TimeError;
                table.Port = listRemotePoints[i].Port;
                table.IpAddress = listRemotePoints[i].IpAddress;
                table.Note = listRemotePoints[i].Note;

                list.Add(table);
            }

            return list;
        }

        /// <summary>
        /// Преобразование MyTableRemotePoints к TableRemotePoints
        /// </summary>
        /// <param name="tableRemotePoints"></param>
        /// <returns> List<TableJammerStation> </returns>
        private TableRemotePoints MyTableRemotePointsToTableRemotePoints(MyTableRemotePoints tableRemotePoints)
        {
            TableRemotePoints table = new TableRemotePoints();

            table.Id = tableRemotePoints.Id;
            table.CourseAngle = tableRemotePoints.CourseAngle;
            table.TimeError = tableRemotePoints.TimeError;
            table.Port = tableRemotePoints.Port;
            table.IpAddress = tableRemotePoints.IpAddress;
            table.Note = tableRemotePoints.Note;

            switch (PropViewCoords.ViewCoords)
            {
                case 1: // format "DD.dddddd"
                    table.Coordinates.Altitude = tableRemotePoints.Coordinates.Altitude;
                    table.Coordinates.Latitude = tableRemotePoints.Coordinates.Latitude;
                    table.Coordinates.Longitude = tableRemotePoints.Coordinates.Longitude;
                    break;

                case 2: // format "DD MM.mmmm"
                    table.Coordinates.Altitude = tableRemotePoints.CoordinatesDDMMSS.Altitude;
                    table.Coordinates.Latitude = Math.Round(tableRemotePoints.CoordinatesDDMMSS.LatDegrees + ((double)tableRemotePoints.CoordinatesDDMMSS.LatMinutesDDMM / 60), 6);
                    table.Coordinates.Longitude = Math.Round(tableRemotePoints.CoordinatesDDMMSS.LonDegrees + ((double)tableRemotePoints.CoordinatesDDMMSS.LonMinutesDDMM / 60), 6);
                    break;

                case 3: // format "DD MM SS.ss"

                    table.Coordinates.Altitude = tableRemotePoints.Coordinates.Altitude;
                    table.Coordinates.Latitude = Math.Round(tableRemotePoints.CoordinatesDDMMSS.LatDegrees + ((double)tableRemotePoints.CoordinatesDDMMSS.LatMinutesDDMMSS / 60) + (tableRemotePoints.CoordinatesDDMMSS.LatSeconds / 3600), 6);
                    table.Coordinates.Longitude = Math.Round(tableRemotePoints.CoordinatesDDMMSS.LonDegrees + ((double)tableRemotePoints.CoordinatesDDMMSS.LonMinutesDDMMSS / 60) + (tableRemotePoints.CoordinatesDDMMSS.LonSeconds / 3600), 6);
                    break;
                default:
                    break;
            }

            return table;
        }

        /// <summary>
        /// Добавить несколько записей в контрол
        /// </summary>
        /// <param name="listRemotePoints"></param>
        public void AddRemotePoints(List<TableRemotePoints> listRemotePoints)
        {
            //try
            //{
            //    DeleteEmptyRows();

            //    for (int i = 0; i < listRemotePoints.Count; i++)
            //    {
            //        int ind = ((GlobalRemotePoints)DgvRemotePoints.DataContext).CollectionRemotePoints.ToList().FindIndex(x => x.Id == listRemotePoints[i].Id);
            //        if (ind != -1)
            //        {
            //            ((GlobalRemotePoints)DgvRemotePoints.DataContext).CollectionRemotePoints[ind] = listRemotePoints[i];
            //        }
            //        else
            //        {
            //            ((GlobalRemotePoints)DgvRemotePoints.DataContext).CollectionRemotePoints.Add(listRemotePoints[i]);
            //        }
            //    }

            //    AddEmptyRows();
            //}
            //catch { }
        }

        /// <summary>
        /// Удалить одну запись из контрола
        /// </summary>
        public void DeleteRemotePoints(TableRemotePoints tableRemotePoints)
        {
            try
            {
                int index = ((GlobalRemotePoints)DgvRemotePoints.DataContext).CollectionRemotePoints.ToList().FindIndex(x => x.Id == tableRemotePoints.Id);
                ((GlobalRemotePoints)DgvRemotePoints.DataContext).CollectionRemotePoints.RemoveAt(index);

                AddEmptyRows();
            }
            catch { }
        }

        /// <summary>
        /// Удалить все записи из контрола
        /// </summary>
        public void ClearRemotePoints()
        {
            try
            {
                ((GlobalRemotePoints)DgvRemotePoints.DataContext).CollectionRemotePoints.Clear();

                AddEmptyRows();
            }
            catch { }
        }

        /// <summary>
        /// Добавить в таблицу пустые строки
        /// </summary>
        private void AddEmptyRows()
        {
            try
            {
                int сountRowsAll = DgvRemotePoints.Items.Count; // количество всех строк в таблице
                double hs = 23; // высота строки
                double ah = DgvRemotePoints.ActualHeight; // визуализированная высота dataGrid
                double chh = DgvRemotePoints.ColumnHeaderHeight; // высота заголовка

                int countRows = Convert.ToInt32(Math.Floor(Convert.ToDouble(ah) - Convert.ToDouble(chh)) / hs); // количество строк, которое помещается в область видимости = (высота таблицы - заголовок таблицы) / высота строки

                int count = сountRowsAll - countRows; // сколько пустых строк нужно удалить
                int index = -1;
                for (int i = 0; i < count; i++)
                {
                    // Удалить пустые строки в dgv
                    index = ((GlobalRemotePoints)DgvRemotePoints.DataContext).CollectionRemotePoints.ToList().FindIndex(x => x.Id < 0);
                    if (index != -1)
                    {
                        ((GlobalRemotePoints)DgvRemotePoints.DataContext).CollectionRemotePoints.RemoveAt(index);
                    }
                }

                List<MyTableRemotePoints> listRP = new List<MyTableRemotePoints>();
                for (int i = 0; i < countRows - сountRowsAll; i++)
                {
                    MyTableRemotePoints strRP = new MyTableRemotePoints
                    {
                        Id = -2,
                        Coordinates = new Coord() { Altitude = -2, Latitude = -2, Longitude = -2 },
                        CourseAngle = -2,
                        TimeError = -2,
                        IpAddress = string.Empty,
                        Port = -2,
                        Note = string.Empty
                    };
                    listRP.Add(strRP);
                }

                for (int i = 0; i < listRP.Count; i++)
                {
                    ((GlobalRemotePoints)DgvRemotePoints.DataContext).CollectionRemotePoints.Add(listRP[i]);
                }
            }
            catch { }
        }

        /// <summary>
        /// Удалить пустые строки из таблицы
        /// </summary>
        private void DeleteEmptyRows()
        {
            try
            {
                int countEmptyRows = ((GlobalRemotePoints)DgvRemotePoints.DataContext).CollectionRemotePoints.Count(s => s.Id < 0);
                int countAllRows = ((GlobalRemotePoints)DgvRemotePoints.DataContext).CollectionRemotePoints.Count;
                int iCount = countAllRows - countEmptyRows;
                for (int i = iCount; i < countAllRows; i++)
                {
                    ((GlobalRemotePoints)DgvRemotePoints.DataContext).CollectionRemotePoints.RemoveAt(iCount);
                }
            }
            catch { }
        }

        private bool IsSelectedRowEmpty()
        {
            try
            {
                if (((TableRemotePoints)DgvRemotePoints.SelectedItem).Id == -2)
                    return false;
            }
            catch { }

            return true;
        }

        public void SetRemotePointsToPG(Coord coord)
        {
            try
            {
                if (RemotePointsWindow != null)
                {
                    double minutesLat = (coord.Latitude - Math.Truncate(coord.Latitude)) * 60;
                    double minutesLon = (coord.Longitude - Math.Truncate(coord.Longitude)) * 60;

                    switch (PropViewCoords.ViewCoords)
                    {
                        case 1: // format "DD.dddddd"
                            RemotePointsWindow.RemotePoints.Coordinates.Latitude = coord.Latitude;
                            RemotePointsWindow.RemotePoints.Coordinates.Longitude = coord.Longitude;
                            break;

                        case 2: // format "DD MM.mmmm"
                            RemotePointsWindow.RemotePoints.CoordinatesDDMMSS.LatDegrees = (int)Math.Truncate(coord.Latitude);
                            RemotePointsWindow.RemotePoints.CoordinatesDDMMSS.LonDegrees = (int)Math.Truncate(coord.Longitude);
                            RemotePointsWindow.RemotePoints.CoordinatesDDMMSS.LatMinutesDDMM = Math.Round((coord.Latitude - Math.Truncate(coord.Latitude)) * 60, 4);
                            RemotePointsWindow.RemotePoints.CoordinatesDDMMSS.LonMinutesDDMM = Math.Round((coord.Longitude - Math.Truncate(coord.Longitude)) * 60, 4);
                            break;

                        case 3: // format "DD MM SS.ss"
                            RemotePointsWindow.RemotePoints.CoordinatesDDMMSS.LatDegrees = (int)Math.Truncate(coord.Latitude);
                            RemotePointsWindow.RemotePoints.CoordinatesDDMMSS.LonDegrees = (int)Math.Truncate(coord.Longitude);
                            RemotePointsWindow.RemotePoints.CoordinatesDDMMSS.LatMinutesDDMMSS = (int)((coord.Latitude - Math.Truncate(coord.Latitude)) * 60);
                            RemotePointsWindow.RemotePoints.CoordinatesDDMMSS.LonMinutesDDMMSS = (int)((coord.Longitude - Math.Truncate(coord.Longitude)) * 60);
                            RemotePointsWindow.RemotePoints.CoordinatesDDMMSS.LatSeconds = Math.Round((minutesLat - Math.Truncate(minutesLat)) * 60, 2);
                            RemotePointsWindow.RemotePoints.CoordinatesDDMMSS.LonSeconds = Math.Round((minutesLon - Math.Truncate(minutesLon)) * 60, 2);

                            break;

                        default:
                            break;
                    }
                }
            }
            catch { }
        }

    }
}
