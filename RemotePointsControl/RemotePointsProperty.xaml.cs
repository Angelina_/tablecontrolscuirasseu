﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;
using System.Windows.Controls.WpfPropertyGrid;
using System.Windows.Input;
using System.Windows.Media.Imaging;
using TableEvents;
using ValuesCorrectLib;

namespace RemotePointsControl
{
    using System.IO;

    using CuirassUModelsDBLib;

    /// <summary>
    /// Логика взаимодействия для RemotePointsProperty.xaml
    /// </summary>
    public partial class RemotePointsProperty : Window
    {
        public event EventHandler<MyTableRemotePoints> OnAddRecordPG = (object sender, MyTableRemotePoints data) => { };
        public event EventHandler<MyTableRemotePoints> OnChangeRecordPG = (object sender, MyTableRemotePoints data) => { };


        private ObservableCollection<MyTableRemotePoints> collectionTemp;
        public MyTableRemotePoints RemotePoints { get; private set; }

        public RemotePointsProperty(ObservableCollection<MyTableRemotePoints> collectionRemotePoints)
        {
            try
            {
                InitializeComponent();

                InitEditors();

                collectionTemp = collectionRemotePoints;
                RemotePoints = new MyTableRemotePoints();
                propertyGrid.SelectedObject = RemotePoints;

                switch (PropViewCoords.ViewCoords)
                {
                    case 1: // format "DD.dddddd"
                        propertyGrid.Properties[nameof(MyTableRemotePoints.CoordinatesDDMMSS)].IsBrowsable = false;
                        break;

                    case 2: // format "DD MM.mmmm"
                    case 3: // format "DD MM SS.ss"
                        propertyGrid.Properties[nameof(MyTableRemotePoints.Coordinates)].IsBrowsable = false;
                        break;

                    default:
                        break;
                }

                //Title = "Add record";
                Title = SMeaning.meaningAddRecord;
                Icon = new BitmapImage(new Uri(@"pack://application:,,,/"
                                    + System.Reflection.Assembly.GetExecutingAssembly().GetName().Name
                                    + ";component/"
                                    + "Resources/AddRec.ico", UriKind.Absolute));

                InitProperty();
                propertyGrid.Categories.FirstOrDefault(t => t.Name == "Coordinates").HeaderCategoryName = SMeaning.meaningCoord;


                //ChangeCategories();
            }
            catch { }
        }

        public RemotePointsProperty(ObservableCollection<MyTableRemotePoints> collectionRemotePoints, MyTableRemotePoints tableRemotePoints)
        {
            try
            {
                InitializeComponent();

                InitEditors();

                collectionTemp = collectionRemotePoints;
                RemotePoints = tableRemotePoints;

                if (RemotePoints.Coordinates.Latitude != -1)
                {
                    RemotePoints.Coordinates.Latitude = RemotePoints.Coordinates.Latitude < 0 ? RemotePoints.Coordinates.Latitude * -1 : RemotePoints.Coordinates.Latitude;
                    RemotePoints.Coordinates.Latitude = Math.Round(RemotePoints.Coordinates.Latitude, 6);
                }
                if (RemotePoints.Coordinates.Longitude != -1)
                {
                    RemotePoints.Coordinates.Longitude = RemotePoints.Coordinates.Longitude < 0 ? RemotePoints.Coordinates.Longitude * -1 : RemotePoints.Coordinates.Longitude;
                    RemotePoints.Coordinates.Longitude = Math.Round(RemotePoints.Coordinates.Longitude, 6);
                }

                propertyGrid.SelectedObject = RemotePoints;

                switch (PropViewCoords.ViewCoords)
                {
                    case 1: // format "DD.dddddd"
                        propertyGrid.Properties[nameof(MyTableRemotePoints.CoordinatesDDMMSS)].IsBrowsable = false;
                        break;

                    case 2: // format "DD MM.mmmm"
                    case 3: // format "DD MM SS.ss"
                        propertyGrid.Properties[nameof(MyTableRemotePoints.Coordinates)].IsBrowsable = false;
                        break;

                    default:
                        break;
                }

                //Title = "Change record";
                Title = SMeaning.meaningChangeRecord;
                Icon = new BitmapImage(new Uri(@"pack://application:,,,/"
                                    + System.Reflection.Assembly.GetExecutingAssembly().GetName().Name
                                    + ";component/"
                                    + "Resources/ChangeRec.ico", UriKind.Absolute));

                //ChangeCategories();
                InitProperty();
                propertyGrid.Categories.FirstOrDefault(t => t.Name == "Coordinates").HeaderCategoryName = SMeaning.meaningCoord;

            }
            catch { }
        }

        public RemotePointsProperty()
        {
            InitializeComponent();

            InitEditors();
            //ChangeCategories();
            InitProperty();
        }


        private void ButtonApply_Click(object sender, RoutedEventArgs e)
        {
            if (IsAddClick((MyTableRemotePoints)propertyGrid.SelectedObject) != null)
            {
                if (PropIsRecRemotePoints.IsRecAdd)
                {
                    OnAddRecordPG?.Invoke(sender, (MyTableRemotePoints)propertyGrid.SelectedObject);
                }

                if (PropIsRecRemotePoints.IsRecChange)
                {
                    OnChangeRecordPG?.Invoke(sender, (MyTableRemotePoints)propertyGrid.SelectedObject);
                }

                Close();
            }
        }

        public MyTableRemotePoints IsAddClick(MyTableRemotePoints RemotePointsWindow)
        {
            return RemotePointsWindow;
        }

        private void ButtonNoApply_Click(object sender, RoutedEventArgs e)
        {
            Close();
            PropIsRecRemotePoints.IsRecAdd = false;
            PropIsRecRemotePoints.IsRecChange = false;
        }

        private void InitEditors()
        {
            switch (PropViewCoords.ViewCoords)
            {
                case 1: // format "DD.dddddd"
                    propertyGrid.Editors.Add(new CoordinatesDDRemotePointsEditor(nameof(RemotePoints.Coordinates), typeof(MyTableRemotePoints)));
                    //propertyGrid.Editors.Add(new TimeErrorDDRemotePointsEditor(nameof(RemotePoints.TimeError), typeof(MyTableRemotePoints)));
                    break;

                case 2: // format "DD MM.mmmm"
                    propertyGrid.Editors.Add(new CoordinatesDDMMRemotePointsEditor(nameof(RemotePoints.CoordinatesDDMMSS), typeof(MyTableRemotePoints)));
                    //propertyGrid.Editors.Add(new TimeErrorDDMMRemotePointsEditor(nameof(RemotePoints.TimeError), typeof(MyTableRemotePoints)));
                    break;

                case 3: // format "DD MM SS.ss"
                    propertyGrid.Editors.Add(new CoordinatesDDMMSSRemotePointsEditor(nameof(RemotePoints.CoordinatesDDMMSS), typeof(MyTableRemotePoints)));
                    //propertyGrid.Editors.Add(new TimeErrorDDMMRemotePointsEditor(nameof(RemotePoints.TimeError), typeof(MyTableRemotePoints)));
                    break;

                default:
                    break;
            }
        }

        private void InitProperty()
        {
            foreach (var property in propertyGrid.Properties)
            {
                try
                {
                    if (property.IsBrowsable == false) { continue; }

                    if (property.PropertyValue.SubProperties.Count != 0)
                    {
                        foreach (var subProperty in property.PropertyValue.SubProperties)
                            subProperty.PropertyValue.PropertyValueException += PropertyGridSubException;
                        continue;
                    }
                    property.PropertyValue.PropertyValueException += PropertyGridException;
                }
                catch (Exception ex)
                { System.Windows.MessageBox.Show(ex.Message); }
            }
        }

        private void PropertyGridSubException(object sender, ValueExceptionEventArgs e)
        {
            propertyGrid.Properties[(sender as PropertyItemValue).ParentProperty.ParentValue.ParentProperty.Name].SetValue((sender as PropertyItemValue).Value);
        }

        private void PropertyGridException(object sender, ValueExceptionEventArgs e)
        {
            propertyGrid.Properties[(sender as PropertyItemValue).ParentProperty.Name].SetValue((sender as PropertyItemValue).Value);
        }

        private void gridProperty_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == System.Windows.Input.Key.Enter)
            {
                if (IsAddClick((MyTableRemotePoints)propertyGrid.SelectedObject) != null)
                {
                    if (PropIsRecRemotePoints.IsRecAdd)
                    {
                        OnAddRecordPG?.Invoke(sender, (MyTableRemotePoints)propertyGrid.SelectedObject);
                    }

                    if (PropIsRecRemotePoints.IsRecChange)
                    {
                        OnChangeRecordPG?.Invoke(sender, (MyTableRemotePoints)propertyGrid.SelectedObject);
                    }

                    Close();
                }
            }
        }

        public void SetLanguagePropertyGrid(Languages language)
        {
            LoadTranslatorPropertyGrid(language);
            TranslatorTables.ChangeLanguagePropertyGrid(language, propertyGrid);
        }

        private void LoadTranslatorPropertyGrid(Languages language)
        {
            ResourceDictionary dict = new ResourceDictionary();
            try
            {
                var path = Directory.GetParent(Environment.CurrentDirectory).Parent.Parent.FullName.Split(Path.DirectorySeparatorChar).Last();
                switch (language)
                {
                    case Languages.Eng:
                        dict.Source = new Uri($"/{path};component/Languages/TranslatorTables/TranslatorTablesCuirasse.EN.xaml",
                            UriKind.Relative);
                        break;
                    case Languages.Rus:

                        dict.Source = new Uri($"/{path};component/Languages/TranslatorTables/TranslatorTablesCuirasse.RU.xaml",
                            UriKind.Relative);
                        break;
                    default:
                        dict.Source = new Uri($"/{path};component/Languages/TranslatorTables/TranslatorTablesCuirasse.RU.xaml",
                            UriKind.Relative);
                        break;
                }

                this.Resources.MergedDictionaries.Add(dict);
            }
            catch (Exception ex)
            {
                //MessageBox.Show(ex.Message);
            }
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            PropIsRecRemotePoints.IsRecAdd = false;
            PropIsRecRemotePoints.IsRecChange = false;
        }
    }
}
