﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TableEvents
{
    public interface ITableEventReport
    {
        event EventHandler<TableEventReport> OnAddTableToReport;

    }
}
