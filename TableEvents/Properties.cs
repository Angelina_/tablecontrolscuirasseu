﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TableEvents
{
    public class PropShowDialog
    {
        private static bool isShowDialogOpen = false;
        public static bool IsShowDialogOpen
        {
            get { return isShowDialogOpen; }
            set
            {
                if (isShowDialogOpen == value)
                    return;

                isShowDialogOpen = value;
            }
        }
    }

    public class PropNumRES
    {
        //private static int selectedNumRES = 2;// для отладки
        private static int selectedNumRES = 0;
        public static int SelectedNumRES
        {
            get { return selectedNumRES; }
            set
            {
                if (selectedNumRES == value)
                    return;

                selectedNumRES = value;
            }
        }

        //private static bool isSelectedNumRES = true; // для отладки
        private static bool isSelectedNumRES = false;
        public static bool IsSelectedNumRES
        {
            get { return isSelectedNumRES; }
            set
            {
                if (isSelectedNumRES == value)
                    return;

                isSelectedNumRES = value;
            }
        }
    }

    public class PropNumRESPattern
    {
        //private static int selectedNumRESPattern = 2;// для отладки
        private static int selectedNumRESPattern = 0;
        public static int SelectedNumRESPattern
        {
            get { return selectedNumRESPattern; }
            set
            {
                if (selectedNumRESPattern == value)
                    return;

                selectedNumRESPattern = value;
            }
        }

        //private static bool isSelectedNumRESPattern = true; // для отладки
        private static bool isSelectedNumRESPattern = false;
        public static bool IsSelectedNumRESPattern
        {
            get { return isSelectedNumRESPattern; }
            set
            {
                if (isSelectedNumRESPattern == value)
                    return;

                isSelectedNumRESPattern = value;
            }
        }
    }


    public class PropNumRESDistribution
    {
        //private static int selectedNumRES = 2;// для отладки
        private static int selectedNumRESDistribution = 0;
        public static int SelectedNumRESDistribution
        {
            get { return selectedNumRESDistribution; }
            set
            {
                if (selectedNumRESDistribution == value)
                    return;

                selectedNumRESDistribution = value;
            }
        }

        //private static bool isSelectedNumRESDistribution = true; // для отладки
        private static bool isSelectedNumRESDistribution = false;
        public static bool IsSelectedNumRESDistribution
        {
            get { return isSelectedNumRESDistribution; }
            set
            {
                if (isSelectedNumRESDistribution == value)
                    return;

                isSelectedNumRESDistribution = value;
            }
        }
    }

    public class PropNumRESArchive
    {
        //private static int selectedNumRESArchive = 2;// для отладки
        private static int selectedNumRESArchive = 0;
        public static int SelectedNumRESArchive
        {
            get { return selectedNumRESArchive; }
            set
            {
                if (selectedNumRESArchive == value)
                    return;

                selectedNumRESArchive = value;
            }
        }

        //private static bool isSelectedNumRESArchive = true; // для отладки
        private static bool isSelectedNumRESArchive = false;
        public static bool IsSelectedNumRESArchive
        {
            get { return isSelectedNumRESArchive; }
            set
            {
                if (isSelectedNumRESArchive == value)
                    return;

                isSelectedNumRESArchive = value;
            }
        }
    }

    public class PropNumRemotePoints
    {
        //private static int selectedNumRemotePoints = 2;// для отладки
        private static int selectedNumRemotePoints = 0;
        public static int SelectedNumRemotePoints
        {
            get { return selectedNumRemotePoints; }
            set
            {
                if (selectedNumRemotePoints == value)
                    return;

                selectedNumRemotePoints = value;
            }
        }

        //private static bool isSelectedNumRemotePoints = true; // для отладки
        private static bool isSelectedNumRemotePoints = false;
        public static bool IsSelectedNumRemotePoints
        {
            get { return isSelectedNumRemotePoints; }
            set
            {
                if (isSelectedNumRemotePoints == value)
                    return;

                isSelectedNumRemotePoints = value;
            }
        }
    }
    public class PropIsRecRemotePoints
    {
        private static bool isRecAdd = false;
        public static bool IsRecAdd
        {
            get { return isRecAdd; }
            set
            {
                if (isRecAdd == value)
                    return;

                isRecAdd = value;
            }
        }

        private static bool isRecChange = false;
        public static bool IsRecChange
        {
            get { return isRecChange; }
            set
            {
                if (isRecChange == value)
                    return;

                isRecChange = value;
            }
        }
    }
    public class PropViewCoords
    {
        private static byte viewCoords = 1;
        public static byte ViewCoords
        {
            get { return viewCoords; }
            set
            {
                if (viewCoords == value)
                    return;

                viewCoords = value;
            }
        }
    }

}
