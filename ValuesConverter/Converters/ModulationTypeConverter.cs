﻿using CuirassUModelsDBLib;
using System;
using System.Globalization;
using System.Windows.Data;

namespace ValuesConverter
{
    public class ModulationTypeConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            //try
            //{
            string sModulationType = string.Empty;

            switch (value)
            {
                case ModulationType.AM:
                    sModulationType = "AM";
                    break;

                case ModulationType.FM:
                    sModulationType = "FM";
                    break;

                case ModulationType.LFM:
                    sModulationType = "LFM";
                    break;

                case ModulationType.UM:
                    sModulationType = "UM";
                    break;

                default:
                    break;
            }

            return sModulationType;
            //}
            //catch { }
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
