﻿using CuirassUModelsDBLib;
using System;
using System.Globalization;
using System.Windows.Data;

namespace ValuesConverter
{
    [ValueConversion(sourceType: typeof(float), targetType: typeof(string))]
    public class ParamDPDurationConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        { 
            string sDuration = string.Empty;

            try
            {
                float fDuration = System.Convert.ToSingle(((ParamDP)value).Duration);

                if (fDuration == -1F)
                {
                    return "-";
                }
                if (fDuration == -2F)
                {
                    return string.Empty;
                }

                // проверка
                if (fDuration > 0.01F && fDuration < 1000F)
                {
                    sDuration = fDuration.ToString("0.00");
                }
                else
                {
                    sDuration = "NI";
                }
            }

            catch { }

            return sDuration;
            //return System.Convert.ToString(System.Convert.ToSingle(((ParamDP)value).Duration));
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    [ValueConversion(sourceType: typeof(float), targetType: typeof(string))]
    public class ParamDPPeriodConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            string sPeriod = string.Empty;

            try
            {
                float fPeriod = System.Convert.ToSingle(((ParamDP)value).Period);

                if (fPeriod == -1F)
                {
                    return "-";
                }
                if (fPeriod == -2F)
                {
                    return string.Empty;
                }

                // проверка
                if (fPeriod > 0.01F && fPeriod < 1000F)
                {
                    sPeriod = fPeriod.ToString("0.00");
                }
                else
                {
                    sPeriod = "NI";
                }
            }

            catch { }

            return sPeriod;
            //return System.Convert.ToString(System.Convert.ToSingle(((ParamDP)value).Period));
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
