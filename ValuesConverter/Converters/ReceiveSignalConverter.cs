﻿using CuirassUModelsDBLib;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;

namespace ValuesConverter
{
    [ValueConversion(sourceType: typeof(ReceiveSignal), targetType: typeof(Uri))]
    public class ReceiveSignalConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            //try
            //{
            Uri uri;
            switch (value)
            {
                case ReceiveSignal.YesSignal:
                    uri = new Uri(@"pack://application:,,,/"
                              + System.Reflection.Assembly.GetExecutingAssembly().GetName().Name
                              + ";component/"
                              + "Resources/green.png", UriKind.Absolute);
                    break;

                case ReceiveSignal.NoSignal:
                    uri = new Uri(@"pack://application:,,,/"
                               + System.Reflection.Assembly.GetExecutingAssembly().GetName().Name
                               + ";component/"
                               + "Resources/red.png", UriKind.Absolute);
                    break;

                case ReceiveSignal.NoAnswer:
                    uri = new Uri(@"pack://application:,,,/"
                               + System.Reflection.Assembly.GetExecutingAssembly().GetName().Name
                               + ";component/"
                               + "Resources/gray.png", UriKind.Absolute);
                    break;

                case ReceiveSignal.BadSignal:
                    uri = new Uri(@"pack://application:,,,/"
                               + System.Reflection.Assembly.GetExecutingAssembly().GetName().Name
                               + ";component/"
                               + "Resources/orange.png", UriKind.Absolute);
                    break;

                default:
                    uri = new Uri(@"pack://application:,,,/"
                              + System.Reflection.Assembly.GetExecutingAssembly().GetName().Name
                              + ";component/"
                              + "Resources/empty.png", UriKind.Absolute);
                    break;
            }

            return uri;
            //}

            //catch { return null; }
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
