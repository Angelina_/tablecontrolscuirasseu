﻿using CuirassUModelsDBLib;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;
using ValuesCorrectLib;

namespace ValuesConverter
{
    public class TargetClassConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            //try
            //{
            string sTargetClass = string.Empty;

            switch (value)
            {
                case ClassTarget.StaticObject:
                    sTargetClass = SMeaning.meaningStaticObject; // "Неизв. объект";
                    break;

                case ClassTarget.UAV:
                    sTargetClass = SMeaning.meaningUAV; // "БПЛА";
                    break;

                case ClassTarget.Aircraft:
                    sTargetClass = SMeaning.meaningAircraft; // "Самолет";
                    break;

                case ClassTarget.Helicopter:
                    sTargetClass = SMeaning.meaningHelicopter; // "Вертолет";
                    break;

                case ClassTarget.Radar:
                    sTargetClass = SMeaning.meaningRadar; // "РЛС";
                    break;

                case ClassTarget.SurvRadar:
                    sTargetClass = SMeaning.meaningSurvRadar; // "РЛС Обзорная";
                    break;

                case ClassTarget.FCRadar:
                    sTargetClass = SMeaning.meaningFCRadar; // "РЛС Упр. Огнём";
                    break;

                case ClassTarget.WCRadar:
                    sTargetClass = SMeaning.meaningWCRadar; // "Метеостанция";
                    break;

                case ClassTarget.LandingRadar:
                    sTargetClass = SMeaning.meaningLandingRadar; // "РЛС Наземная";
                    break;

                default:
                    break;
            }

            return sTargetClass;
            //}
            //catch { }
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    public class TargetClassPropGridConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            byte bRole = (byte)value;

            return (byte)value;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return (ClassTarget)System.Convert.ToByte(value);
        }
    }
}
