﻿using System;
using System.Globalization;
using System.Windows.Data;
using CuirassUModelsDBLib;

namespace ValuesConverter
{
    public class TypeConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            string sType = string.Empty;

            //try
            //{

            if ((byte)value == 255)
                return sType;

            switch (value)
            {
                case TypeUAVRes.Ocusinc:
                    sType = "Mavic 2";
                    break;

                case TypeUAVRes.G3:
                    sType = "3G";
                    break;

                case TypeUAVRes.Lightbridge:
                    sType = "Phantom 4";
                    break;

                case TypeUAVRes.WiFi:
                    sType = "Mavic Air";
                    break;

                case TypeUAVRes.Unknown:
                    sType = "Unknown";
                    break;

                default:
                    sType = string.Empty;
                    break;
            }

            //}
            //catch { }

            return sType;

        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
