﻿using CuirassUModelsDBLib;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;
using ValuesCorrectLib;

namespace ValuesConverter
{
    public class WorkingModeConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            //try
            //{
            string sWorkingMode = string.Empty;

            switch (value)
            {
                case WorkingMode.AirAir:
                    sWorkingMode = SMeaning.meaningAirAir; // "Воздух-Воздух";
                    break;

                case WorkingMode.AirLand:
                    sWorkingMode = SMeaning.meaningAirLand; // "Воздух-Земля";
                    break;

                case WorkingMode.LandLand:
                    sWorkingMode = SMeaning.meaningLandLand; // "Земля-Земля";
                    break;

                default:
                    break;
            }

            return sWorkingMode;
            //}
            //catch { }
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    public class WorkingModePropGridConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            byte bRole = (byte)value;

            return (byte)value;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return (WorkingMode)System.Convert.ToByte(value);
        }
    }
}
