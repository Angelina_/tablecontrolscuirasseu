﻿using CuirassUModelsDBLib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ValuesCorrectLib
{
    using System.Windows;

    public class CorrectParams
    {
        /// <summary>
        /// Проверка на правильность ввода значений:  
        /// если Fmin меньше 100 или больше 18000, Fmin = 100;
        /// если Fmax меньше 100 или больше 18000, Fmax = 18000;
        /// </summary>
        /// <param name="suppressFHSS">Fmin, Fmax</param>
        public static void IsCorrectMinMax(double dFreqMin, double dFreqMax)
        {
            if (dFreqMin < 100.0F || dFreqMin > 18000.0F) { dFreqMin = 100.0F; }
            if (dFreqMax < 100.0F || dFreqMax > 18000.0F) { dFreqMax = 18000.0F; }
        }

        /// <summary>
        /// Проверка на правильность ввода значений:  
        /// если Аmin меньше 0 или больше 360, Аmin = 0;
        /// если Аmax меньше 0 или больше 360, Аmax = 360
        /// </summary>
        /// <param name="sr">Fmin, Fmax, Amin, Amax</param>
        public static void IsCorrectMinMax(TableSectorsRecon sr)
        {
            if (sr.AngleMin < 0 || sr.AngleMin > 360) { sr.AngleMin = 0; }
            if (sr.AngleMax < 0 || sr.AngleMax > 360) { sr.AngleMax = 360; }
        }

        /// <summary>
        /// Проверка на корректность ввода значений частоты
        /// </summary>
        /// <param name="iFreqMin"> начальное значение частоты </param>
        /// <param name="iFreqMax"> конечное значение частоты </param>
        /// <returns> true - успешно, false - нет </returns>
        public static bool IsCorrectFreqMinMax(double dFreqMin, double dFreqMax)
        {
            bool bCorrect = true;

            if (dFreqMin >= dFreqMax)
            {
                MessageBox.Show(SMessages.mesValuesMaxMin, SMessages.mesMessage, MessageBoxButton.OK, MessageBoxImage.Information);

                bCorrect = false;
            }

            return bCorrect;
        }

        /// <summary>
        /// Проверка на корректность ввода значений сектора
        /// </summary>
        /// <param name="iAngleMin"> начальное значение сектора </param>
        /// <param name="iAngleMax"> конечное значение сектора </param>
        /// <returns> true - успешно, false - нет </returns>
        public static bool IsCorrectAngleMinMax(short iAngleMin, short iAngleMax)
        {
            bool bCorrect = true;

            if (iAngleMin >= iAngleMax)
            {
                MessageBox.Show(SMessages.mesValuesAngleMaxMin, SMessages.mesMessage, MessageBoxButton.OK, MessageBoxImage.Information);

                bCorrect = false;
            }

            return bCorrect;
        }
    }
}
