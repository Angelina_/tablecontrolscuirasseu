﻿using System;
using System.Windows;
using System.Windows.Controls.WpfPropertyGrid;

namespace ValuesCorrectLib
{
    public class FreqsForbiddenFminEditor : PropertyEditor
    {
        public FreqsForbiddenFminEditor(string PropertyName, Type DeclaringType)
        {
            this.PropertyName = PropertyName;
            this.DeclaringType = DeclaringType;
            var resource = new ResourceDictionary
            {
                Source = new Uri("/FreqsForbiddenControl;component/Themes/PropertyGridEditorFreqsForbidden.xaml",
                        UriKind.RelativeOrAbsolute)
            };
            InlineTemplate = resource["FreqsForbiddenFminEditorKey"];
        }
    }

    public class FreqsForbiddenFmaxEditor : PropertyEditor
    {
        public FreqsForbiddenFmaxEditor(string PropertyName, Type DeclaringType)
        {
            this.PropertyName = PropertyName;
            this.DeclaringType = DeclaringType;
            var resource = new ResourceDictionary
            {
                Source = new Uri("/FreqsForbiddenControl;component/Themes/PropertyGridEditorFreqsForbidden.xaml",
                        UriKind.RelativeOrAbsolute)
            };
            InlineTemplate = resource["FreqsForbiddenFmaxEditorKey"];
        }
    }

    public class FreqsForbiddenNoteEditor : PropertyEditor
    {
        public FreqsForbiddenNoteEditor(string PropertyName, Type DeclaringType)
        {
            this.PropertyName = PropertyName;
            this.DeclaringType = DeclaringType;
            var resource = new ResourceDictionary
            {
                Source = new Uri("/FreqsForbiddenControl;component/Themes/PropertyGridEditorFreqsForbidden.xaml",
                        UriKind.RelativeOrAbsolute)
            };
            InlineTemplate = resource["FreqsForbiddenNoteEditorKey"];
        }
    }

    public class FreqsForbiddenPulseEditor : PropertyEditor
    {
        public FreqsForbiddenPulseEditor(string PropertyName, Type DeclaringType)
        {
            this.PropertyName = PropertyName;
            this.DeclaringType = DeclaringType;
            var resource = new ResourceDictionary
            {
                Source = new Uri("/FreqsForbiddenControl;component/Themes/PropertyGridEditorFreqsForbidden.xaml",
                        UriKind.RelativeOrAbsolute)
            };
            InlineTemplate = resource["FreqsForbiddenPulseEditorKey"];
        }
    }
}
