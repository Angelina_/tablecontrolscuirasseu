﻿using System;
using System.Windows;
using System.Windows.Controls.WpfPropertyGrid;

namespace ValuesCorrectLib
{
    public class RESPatternFminEditor : PropertyEditor
    {
        public RESPatternFminEditor(string PropertyName, Type DeclaringType)
        {
            this.PropertyName = PropertyName;
            this.DeclaringType = DeclaringType;
            var resource = new ResourceDictionary
            {
                Source = new Uri("/RESPatternControl;component/Themes/PropertyGridEditorRESPattern.xaml",
                        UriKind.RelativeOrAbsolute)
            };
            InlineTemplate = resource["RESPatternFminEditorKey"];
        }
    }

    public class RESPatternFmaxEditor : PropertyEditor
    {
        public RESPatternFmaxEditor(string PropertyName, Type DeclaringType)
        {
            this.PropertyName = PropertyName;
            this.DeclaringType = DeclaringType;
            var resource = new ResourceDictionary
            {
                Source = new Uri("/RESPatternControl;component/Themes/PropertyGridEditorRESPattern.xaml",
                        UriKind.RelativeOrAbsolute)
            };
            InlineTemplate = resource["RESPatternFmaxEditorKey"];
        }
    }

    public class RESPatternPulseEditor : PropertyEditor
    {
        public RESPatternPulseEditor(string PropertyName, Type DeclaringType)
        {
            this.PropertyName = PropertyName;
            this.DeclaringType = DeclaringType;
            var resource = new ResourceDictionary
            {
                Source = new Uri("/RESPatternControl;component/Themes/PropertyGridEditorRESPattern.xaml",
                        UriKind.RelativeOrAbsolute)
            };
            InlineTemplate = resource["RESPatternPulseEditorKey"];
        }
    }

    public class RESPatternSeriesEditor : PropertyEditor
    {
        public RESPatternSeriesEditor(string PropertyName, Type DeclaringType)
        {
            this.PropertyName = PropertyName;
            this.DeclaringType = DeclaringType;
            var resource = new ResourceDictionary
            {
                Source = new Uri("/RESPatternControl;component/Themes/PropertyGridEditorRESPattern.xaml",
                        UriKind.RelativeOrAbsolute)
            };
            InlineTemplate = resource["RESPatternSeriesEditorKey"];
        }
    }

    public class RESPatternWorkingModeEditor : PropertyEditor
    {
        public RESPatternWorkingModeEditor(string PropertyName, Type DeclaringType)
        {
            this.PropertyName = PropertyName;
            this.DeclaringType = DeclaringType;
            var resource = new ResourceDictionary
            {
                Source = new Uri("/RESPatternControl;component/Themes/PropertyGridEditorRESPattern.xaml",
                        UriKind.RelativeOrAbsolute)
            };
            InlineTemplate = resource["RESPatternWorkingModeEditorKey"];
        }
    }

    public class RESPatternTargetClassEditor : PropertyEditor
    {
        public RESPatternTargetClassEditor(string PropertyName, Type DeclaringType)
        {
            this.PropertyName = PropertyName;
            this.DeclaringType = DeclaringType;
            var resource = new ResourceDictionary
            {
                Source = new Uri("/RESPatternControl;component/Themes/PropertyGridEditorRESPattern.xaml",
                        UriKind.RelativeOrAbsolute)
            };
            InlineTemplate = resource["RESPatternTargetClassEditorKey"];
        }
    }
}
