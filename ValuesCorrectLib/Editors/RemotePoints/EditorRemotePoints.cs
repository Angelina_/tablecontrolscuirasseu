﻿using System;
using System.Windows;
using System.Windows.Controls.WpfPropertyGrid;

namespace ValuesCorrectLib
{
    #region Задание координат в градусах (DD)
    public class CoordinatesDDRemotePointsEditor : PropertyEditor
    {
        public CoordinatesDDRemotePointsEditor(string PropertyName, Type DeclaringType)
        {
            this.PropertyName = PropertyName;
            this.DeclaringType = DeclaringType;
            var resource = new ResourceDictionary
            {
                Source = new Uri("/RemotePointsControl;component/Themes/PropertyGridEditorRemotePointsDD.xaml",
                        UriKind.RelativeOrAbsolute)
            };
            InlineTemplate = resource["CoordinatesDDRemotePointsEditorKey"];
        }
    }
    public class TimeErrorDDRemotePointsEditor : PropertyEditor
    {
        public TimeErrorDDRemotePointsEditor(string PropertyName, Type DeclaringType)
        {
            this.PropertyName = PropertyName;
            this.DeclaringType = DeclaringType;
            var resource = new ResourceDictionary
            {
                Source = new Uri("/RemotePointsControl;component/Themes/PropertyGridEditorRemotePointsDD.xaml",
                        UriKind.RelativeOrAbsolute)
            };
            InlineTemplate = resource["TimeErrorDDRemotePointsEditorKey"];
        }
    }

    #endregion

    #region Задание координат в градусах (DDMM)
    public class CoordinatesDDMMRemotePointsEditor : PropertyEditor
    {
        public CoordinatesDDMMRemotePointsEditor(string PropertyName, Type DeclaringType)
        {
            this.PropertyName = PropertyName;
            this.DeclaringType = DeclaringType;
            var resource = new ResourceDictionary
            {
                Source = new Uri("/RemotePointsControl;component/Themes/PropertyGridEditorRemotePointsDDMM.xaml",
                        UriKind.RelativeOrAbsolute)
            };
            InlineTemplate = resource["CoordinatesDDMMRemotePointsEditorKey"];
        }
    }

    public class TimeErrorDDMMRemotePointsEditor : PropertyEditor
    {
        public TimeErrorDDMMRemotePointsEditor(string PropertyName, Type DeclaringType)
        {
            this.PropertyName = PropertyName;
            this.DeclaringType = DeclaringType;
            var resource = new ResourceDictionary
            {
                Source = new Uri("/RemotePointsControl;component/Themes/PropertyGridEditorRemotePointsDDMM.xaml",
                        UriKind.RelativeOrAbsolute)
            };
            InlineTemplate = resource["TimeErrorDDMMRemotePointsEditorKey"];
        }
    }
    #endregion

    #region Задание координат в градусах (DDMMSS)
    public class CoordinatesDDMMSSRemotePointsEditor : PropertyEditor
    {
        public CoordinatesDDMMSSRemotePointsEditor(string PropertyName, Type DeclaringType)
        {
            this.PropertyName = PropertyName;
            this.DeclaringType = DeclaringType;
            var resource = new ResourceDictionary
            {
                Source = new Uri("/RemotePointsControl;component/Themes/PropertyGridEditorRemotePointsDDMMSS.xaml",
                        UriKind.RelativeOrAbsolute)
            };
            InlineTemplate = resource["CoordinatesDDMMSSRemotePointsEditorKey"];
        }
    }

    public class TimeErrorDDMMSSRemotePointsEditor : PropertyEditor
    {
        public TimeErrorDDMMSSRemotePointsEditor(string PropertyName, Type DeclaringType)
        {
            this.PropertyName = PropertyName;
            this.DeclaringType = DeclaringType;
            var resource = new ResourceDictionary
            {
                Source = new Uri("/RemotePointsControl;component/Themes/PropertyGridEditorRemotePointsDDMMSS.xaml",
                        UriKind.RelativeOrAbsolute)
            };
            InlineTemplate = resource["TimeErrorDDMMSSRemotePointsEditorKey"];
        }
    }
    #endregion
}
