﻿using System.Collections.Generic;

namespace ValuesCorrectLib
{
    public class FunctionsTranslate
    {
        /// <summary>
        /// Переименование сообщений при смене языка
        /// </summary>
        /// <param name="Dictionary"></param>
        public static void RenameMessages(Dictionary<string, string> TranslateDic)
        {
            if (TranslateDic.ContainsKey("mesMessage"))
                SMessages.mesMessage = TranslateDic["mesMessage"];

            if (TranslateDic.ContainsKey("mesValuesMaxMin"))
                SMessages.mesValuesMaxMin = TranslateDic["mesValuesMaxMin"];

            if (TranslateDic.ContainsKey("mesValuesAngleMaxMin"))
                SMessages.mesValuesAngleMaxMin = TranslateDic["mesValuesAngleMaxMin"];

            if (TranslateDic.ContainsKey("mesErr"))
                SMessageError.mesErr = TranslateDic["mesErr"];

            if (TranslateDic.ContainsKey("mesErrValBandwidth"))
                SMessageError.mesErrValBandwidth = TranslateDic["mesErrValBandwidth"];

            if (TranslateDic.ContainsKey("mesErrEnterValues"))
                SMessageError.mesErrEnterValues = TranslateDic["mesErrEnterValues"];
        }

        /// <summary>
        /// Переименование значений при смене языка
        /// </summary>
        /// <param name="TranslateDic"></param>
        public static void RenameMeaning(Dictionary<string, string> TranslateDic)
        {
            if (TranslateDic.ContainsKey("meaningAddRecord"))
                SMeaning.meaningAddRecord = TranslateDic["meaningAddRecord"];

            if (TranslateDic.ContainsKey("meaningChangeRecord"))
                SMeaning.meaningChangeRecord = TranslateDic["meaningChangeRecord"];

             if (TranslateDic.ContainsKey("meaningHz"))
                SMeaning.meaningHz = TranslateDic["meaningHz"];
 
            if (TranslateDic.ContainsKey("meaningkHz"))
                SMeaning.meaningkHz = TranslateDic["meaningkHz"];

            if (TranslateDic.ContainsKey("meaningMHz"))
                SMeaning.meaningMHz = TranslateDic["meaningMHz"];

            if (TranslateDic.ContainsKey("meaningmks"))
                SMeaning.meaningmks = TranslateDic["meaningmks"];

            if (TranslateDic.ContainsKey("meaningms"))
                SMeaning.meaningms = TranslateDic["meaningms"];

            if (TranslateDic.ContainsKey("meaningCoord"))
                SMeaning.meaningCoord = TranslateDic["meaningCoord"];

            if (TranslateDic.ContainsKey("meaningAirAir"))
                SMeaning.meaningAirAir = TranslateDic["meaningAirAir"];

            if (TranslateDic.ContainsKey("meaningAirLand"))
                SMeaning.meaningAirLand = TranslateDic["meaningAirLand"];

            if (TranslateDic.ContainsKey("meaningLandLand"))
                SMeaning.meaningLandLand = TranslateDic["meaningLandLand"];

            if (TranslateDic.ContainsKey("meaningStaticObject"))
                SMeaning.meaningStaticObject = TranslateDic["meaningStaticObject"];

            if (TranslateDic.ContainsKey("meaningUAV"))
                SMeaning.meaningUAV = TranslateDic["meaningUAV"];

            if (TranslateDic.ContainsKey("meaningAircraft"))
                SMeaning.meaningAircraft = TranslateDic["meaningAircraft"];

            if (TranslateDic.ContainsKey("meaningHelicopter"))
                SMeaning.meaningHelicopter = TranslateDic["meaningHelicopter"];

            if (TranslateDic.ContainsKey("meaningRadar"))
                SMeaning.meaningRadar = TranslateDic["meaningRadar"];

            if (TranslateDic.ContainsKey("meaningSurvRadar"))
                SMeaning.meaningSurvRadar = TranslateDic["meaningSurvRadar"];

            if (TranslateDic.ContainsKey("meaningFCRadar"))
                SMeaning.meaningFCRadar = TranslateDic["meaningFCRadar"];

            if (TranslateDic.ContainsKey("meaningWCRadar"))
                SMeaning.meaningWCRadar = TranslateDic["meaningWCRadar"];

            if (TranslateDic.ContainsKey("meaningLandingRadar"))
                SMeaning.meaningLandingRadar = TranslateDic["meaningLandingRadar"];
        }
       
        /// <summary>
        /// Переименование заголовков для вывода в текстовый документ при смене языка
        /// </summary>
        /// <param name="TranslateDic"></param>
        public static void RenameHeaders(Dictionary<string, string> TranslateDic)
        {
            if (TranslateDic.ContainsKey("headerLatLon"))
                SHeaders.headerLatLon = TranslateDic["headerLatLon"];

            if (TranslateDic.ContainsKey("headerAlt"))
                SHeaders.headerAlt = TranslateDic["headerAlt"];

            if (TranslateDic.ContainsKey("headerFreqMin"))
                SHeaders.headerFreqMin = TranslateDic["headerFreqMin"];

            if (TranslateDic.ContainsKey("headerFreqMax"))
                SHeaders.headerFreqMax = TranslateDic["headerFreqMax"];

            if (TranslateDic.ContainsKey("headerNote"))
                SHeaders.headerNote = TranslateDic["headerNote"];

            if (TranslateDic.ContainsKey("headerDeltaF"))
                SHeaders.headerDeltaF = TranslateDic["headerDeltaF"];

            if (TranslateDic.ContainsKey("headerNum"))
                SHeaders.headerNum = TranslateDic["headerNum"];

            if (TranslateDic.ContainsKey("headerFreq"))
                SHeaders.headerFreq = TranslateDic["headerFreq"];

            if (TranslateDic.ContainsKey("headerCount"))
                SHeaders.headerCount = TranslateDic["headerCount"];
        }
    }
}
